package com.jcbase.core.util;

import com.jfinal.plugin.redis.Redis;

import redis.clients.jedis.Jedis;

/**
 * @category redis扩展
 * 
 * @author yiz
 * @date 2016年8月4日 下午3:26:10
 * @version 1.0.0 
 * @copyright  
 */
public class RedisExt {

	/**
	 * @category 用作消息队列
	 * @param key
	 * @param values
	 * @return
	 */
	public static Long lpush(String key, String... values) {
		Jedis jedis = Redis.use().getJedis();
		try {
			return jedis.lpush(key, values);
		} finally {
			Redis.use().close(jedis);
		}
	}
}
