package com.jcbase.controller.sys;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdProductType;
import com.jfinal.plugin.activerecord.Page;

public class ProductTypeController extends JCBaseController {
	@RequiresPermissions(value = { "/sys/productType" })
	public void index() {
		render("product_type_index.jsp");
	}

	@RequiresPermissions(value = { "/sys/productType/save" })
	public void add() {
		String id = getPara("id");
		List<RisebdProductType> productTypes = RisebdProductType.me.getMainProductType();
		setAttr("productTypes", productTypes);
		if (id != null) {
			RisebdProductType productType = RisebdProductType.me.getById(id);
			setAttr("productType", productType);
		}
		this.renderJsp("product_type_add.jsp");
	}

	@RequiresPermissions(value = { "/sys/productType/delete" })
	public void delete() {
		String id = getPara("id");
		//有子级时不可删除
		Map<String, Object> mapData = new HashMap<String, Object>();
		boolean flag = RisebdProductType.me.hasChildType(id);
		if (flag) {
			mapData.put("code", "fail");
			this.renderJson(mapData);
			return;
		}

		boolean result = RisebdProductType.me.delById(id);
		mapData.put("code", "success");
		if (result) {
			mapData.put("result", "删除成功！！");
		} else {
			mapData.put("result", "删除失败！！");
		}
		this.renderJson(mapData);
	}

	public void getByType() {
		Integer type = getParaToInt(0);
		List<RisebdProductType> productTypes = RisebdProductType.me.getByType(1, type);
		Map<String, Object> mapData = new HashMap<String, Object>();
		String name = null;
		if (productTypes.size() > 0) {
			name = productTypes.get(0).getName();
		}
		mapData.put("name", name);
		renderJson(mapData);
	}

	@RequiresPermissions(value = { "/sys/productType/save" })
	public void save() {
		String id = getPara("id");
		String name = getPara("name");
		Integer type = getParaToInt("type");
		Integer type_level = getParaToInt("type_level");
		Integer parent_type = getParaToInt("parent_type");
		String main_img = getPara("main_img");
		String description = getPara("description");
		String mark = getPara("mark");
		List<RisebdProductType> productTypes = RisebdProductType.me.getByType(type_level, type);
		Map<String, Object> mapData = new HashMap<String, Object>();
		int size = productTypes.size();
		if (size > 0 && !CommonUtils.isNotEmpty(id)) {
			mapData.put("code", "fail");
			this.renderJson(mapData);
			return;
		}
		int result = RisebdProductType.me.saveProductType(id, name, type, type_level, parent_type, main_img,
				description, mark);
		mapData.put("code", "success");
		if (result == 1) {
			mapData.put("result", "添加成功！！");
		} else if (result == 2) {
			mapData.put("result", "编辑成功！！");
		}
		this.renderJson(mapData);
	}

	/**
	 * @category 列表
	 */
	public void getListData() {
		String name = this.getPara("name");
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(name)) {
			conditions.add(new Condition("name", Operators.LIKE, name));
		}
		Page<RisebdProductType> pageInfo = RisebdProductType.me.getPage(getPage(), this.getRows(), conditions,
				this.getOrderby());
		List<RisebdProductType> productTypes = pageInfo.getList();
		for (RisebdProductType risebdProductType : productTypes) {
			if (risebdProductType.getTypeLevel() == 2) {
				RisebdProductType type = RisebdProductType.me.getMainProductType(risebdProductType.getParentType());
				risebdProductType.put("parent_type_name", type.getName());
			}
		}
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}
}
