package com.jcbase.controller.sys;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdLink;
import com.jfinal.plugin.activerecord.Page;

public class LinkController extends JCBaseController {
	@RequiresPermissions(value = { "/sys/link" })
	public void index() {
		render("link_index.jsp");
	}

	@RequiresPermissions(value = { "/sys/link/save" })
	public void add() {
		String id = getPara("id");
		if (id != null) {
			RisebdLink risebdLink = RisebdLink.me.getById(id);
			setAttr("risebdLink", risebdLink);
		}
		this.renderJsp("link_add.jsp");
	}

	@RequiresPermissions(value = { "/sys/link/delete" })
	public void delete() {
		String id = getPara("id");
		boolean result = RisebdLink.me.delById(id);
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("code", "success");
		if (result) {
			mapData.put("result", "删除成功！！");
		} else {
			mapData.put("result", "删除失败！！");
		}
		this.renderJson(mapData);
	}

	@RequiresPermissions(value = { "/sys/link/save" })
	public void save() {
		String id = getPara("id");
		String name = getPara("name");
		String link = getPara("link");
		String coverImg = getPara("coverImg");
		Integer type = getParaToInt("type");
		Integer orders = getParaToInt("orders");
		int result = RisebdLink.me.saveLink(id, name, link, coverImg, type, orders);
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("code", "success");
		if (result == 1) {
			mapData.put("result", "添加成功！！");
		} else if (result == 2) {
			mapData.put("result", "编辑成功！！");
		}
		this.renderJson(mapData);
	}

	/**
	 * @category 列表
	 */
	public void getListData() {
		String name = this.getPara("name");
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(name)) {
			conditions.add(new Condition("name", Operators.LIKE, name));
		}
		Page<RisebdLink> pageInfo = RisebdLink.me.getPage(getPage(), this.getRows(), conditions, this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}
}
