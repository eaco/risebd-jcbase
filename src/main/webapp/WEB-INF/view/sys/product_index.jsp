<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Risebd后台管理系统</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- bootstrap & fontawesome -->
		<jsp:include page="/WEB-INF/view/common/basecss.jsp" flush="true" />
	</head>
	<body class="no-skin">
		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
		<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			<div class="main-content" id="page-wrapper">
				<div class="page-content" id="page-content">
					<div class="row">
							<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<div class="widget-box">
								<div class="widget-header widget-header-small">
									<h5 class="widget-title lighter">筛选</h5>
									<span style="padding-left: 92%"><a class="btn btn-success radius r" style="margin-top: 3px; margin-bottom: 3px; line-height: 1.2em;" href="javascript:location.replace(location.href);" title="刷新"><i class="fa fa-refresh"></i></a></span>
								</div>
								<div class="widget-body">
									<div class="widget-main">
											<div class="row">
												<div class="col-xs-12 col-sm-8">
													<div class="row-fluid" style="margin-bottom: 5px;">
														<div class="span12 control-group">
															<jc:button className="btn btn-primary" id="bnt-add" textName="添加" permission="/sys/product/save"/>
															<jc:button className="btn btn-info" id="bnt-edit" textName="编辑" permission="/sys/product/save"/>
															<jc:button className="btn btn-delete" id="bnt-del" textName="删除" permission="/sys/product/delete"/>
														</div>
													</div>
													<div class="input-group">
														<span class="input-group-addon">
															<i class="ace-icon fa fa-check"></i>
														</span>

														<input type="text" id="name" name="name" class="form-control search-query" placeholder="请输入关键字" />
														<span class="input-group-btn">
															<button type="button" id="btn_search" class="btn btn-purple btn-sm">
																<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																搜索
															</button>
														</span>
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="row-fluid" style="margin-bottom: 5px;">
									
								</div>
								<!-- PAGE CONTENT BEGINS -->
								<table id="grid-table"></table>

								<div id="grid-pager"></div>

								<script type="text/javascript">
									var $path_base = "..";//in Ace demo this will be used for editurl parameter
								</script>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div>	
				</div>
			</div>
		</div><!-- /.main-container -->
		<!-- basic scripts -->
		<jsp:include page="/WEB-INF/view/common/basejs.jsp" flush="true" />
			<script type="text/javascript"> 
			function format_type_level(cellvalue, options, rowObject){
				if(1==cellvalue){
					return "主类型";
				}else if(2==cellvalue){
					return "次类型";
				}
				return cellvalue;
			}
			function format_img(cellvalue, options, rowObject){
				return "<img src='${static_url}"+cellvalue+"' width='50px' onerror=\"javascript:this.src='${pageContext.request.contextPath}/res/img/default.jpg'\"/>";
			}
			function formatParent(cellvalue, options, rowObject){
				if(!cellvalue){
					return "";
				}
				var names = '${names}';
				var ids = '${ids}';
				var nameArrs = names.split(',');
				var idArrs = ids.split(',');
				for(var i = 0;i<idArrs.length;i++){
					if(idArrs[i] == cellvalue){
						return nameArrs[i];
					}
				}
				 /* $.ajax({
					 url: "/sys/productType/getByType/"+cellvalue,
					 async: false,
					 success : function(data){
						 return data.name;
					 }
				 }); */
			}
			function formatSecond(cellvalue, options, rowObject){
				if(!cellvalue){
					return "";
				}
				var names = '${names2}';
				var ids = '${ids2}';
				var nameArrs = names.split(',');
				var idArrs = ids.split(',');
				for(var i = 0;i<idArrs.length;i++){
					if(idArrs[i] == cellvalue){
						return nameArrs[i];
					}
				}
				 /* $.ajax({
					 url: "/sys/productType/getByType/"+cellvalue,
					 async: false,
					 success : function(data){
						 return data.name;
					 }
				 }); */
			}
			var selectRowid=-1;
        $(document).ready(function () {
        	var grid_selector = "#grid-table";
			var pager_selector = "#grid-pager";
        	//resize to fit page size
			$(window).on('resize.jqGrid', function () {
				$(grid_selector).jqGrid( 'setGridWidth', $(".page-content").width() );
		    });
//resize on sidebar collapse/expand
				var parent_column = $(grid_selector).closest('[class*="col-"]');
				$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
					if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
						//setTimeout is for webkit only to give time for DOM changes and then redraw!!!
						setTimeout(function() {
							$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
						}, 0);
					}
			    });

            $("#grid-table").jqGrid({
                //url: 'http://trirand.com/blog/phpjqgrid/examples/jsonp/getjsonp.php?callback=?&qwery=longorders',
                url:'/sys/product/getListData',
                mtype: "GET",
                datatype: "json",
                colModel: [
					{ label: 'ID', name: 'id', key: true,hidden:true},
                    { label: '产品名称', name: 'name', width: 90,sortable:false  },
                    { label: '主类', name: 'main_type',width: 75,formatter:formatParent },
                    { label: '次类', name: 'second_type',width: 75,formatter:formatSecond  },
                    { label: '原价(元)', name: 'org_price',width: 30,sortable:false  },
                    { label: '排序', name: 'orders',width: 30,sortable:false  },
                    { label: '主图', name: 'main_img',width: 100,formatter:format_img,sortable:false  },
                    { label: '创建日期', name: 'create_time',width: 75,formatter:'date',
                    	formatoptions:{srcformat: 'Y-m-d H:i:s', newformat: 'Y-m-d H:i:s'} },
                   // { label: '操作', name: 'id',width: 100, sortable:false },
                ],
				viewrecords: true,
                height: 560,
                rowNum: 20,
                sortname:"id",
                sortorder:"desc",
                altRows: true,//隔行变色
                recordtext:"{0} - {1} 共 {2} 条",
                pgtext:"第 {0} 页 共 {1} 页",
                pager:pager_selector,
				onSelectRow : function( rowid ) {
					if(rowid) 
					{
						selectRowid=rowid;
						/**if(rdata.isLeaf === 'true') {
							$("#price").html(rdata.price);
							$("#uiicon").empty().append("<span class='ui-icon "+rdata.uiicon+"'></span>");
						}**/
					} 
				},
                loadComplete : function() {
					var table = this;
					setTimeout(function(){
						updatePagerIcons(table);
					}, 0);
				}
            });
			$(window).triggerHandler('resize.jqGrid');
			
			$("#btn_search").click(function(){  
			    //此处可以添加对查询数据的合法验证  
			    var name = $("#name").val();  
			    $("#grid-table").jqGrid('setGridParam',{  
			        datatype:'json',  
			        postData:{'name':name}, //发送数据  
			        page:1  
			    }).trigger("reloadGrid"); //重新载入  
			}); 
			
			
			$("#bnt-add").click(function(){
				parent.layer.open({
				    type: 2,
				   // shade: [1],
				    fix: false,
				    title: '添加产品',
				    maxmin: true,
				   	content: '/sys/product/add',
				    area: ['780px', '520px']
				}); 
			});
			
			$("#bnt-edit").click(function(){
				var rdata=getSelectedRows();
				var id=rdata.id;
				if (typeof(id) == "undefined") { 
					layer.msg("请选择要编辑的产品");
					return;
				}  
				parent.layer.open({
				    type: 2,
				   // shade: [1],
				    fix: false,
				    title: '编辑产品',
				    maxmin: true,
				   	content: '/sys/product/add?id='+id,
				   	area: ['780px', '520px']
				}); 
			});
			
			$("#bnt-del").click(function(){
				var rdata=getSelectedRows();
				var id=rdata.id;
				if (typeof(id) == "undefined") { 
					layer.msg("请选择要操作的列");
					return;
				}
				layer.confirm('确定要删除此列？删除后不可恢复', {
					  btn: ['我已经肯定了!','容我考虑下~'] //按钮
					}, function(){
						$.post("/sys/product/delete" ,{"id":id},function(data){
			        		if(data.code=='success'){
			        			layer.msg(data.result, {
			        				icon: 1,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			},function(){
			        				 $("#grid-table").trigger("reloadGrid"); //重新载入
			        			});
			        		}
			        		$("#submit-btn").removeClass("disabled");
			        	},"json");
					}, function(){
					  
					});
				
			});
			
        });
        
      //replace icons with FontAwesome icons like above
		function updatePagerIcons(table) {
			var replacement = 
			{
				'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
				'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
				'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
				'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
			};
			$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
			})
		}
		/**获取选中的列***/
		function getSelectedRows() {
			var rdata = $('#grid-table').jqGrid('getRowData', selectRowid);
			return rdata;      
        }
		function setVisible(status){
			alert(getSelectedRows());
		}
		//格式化状态显示
		function fmatterStatus(cellvalue, options, rowObject){
			if(cellvalue==0){
				return '<span class="label label-sm label-warning">禁用</span>';
			}else{
				return '<span class="label label-sm label-success">启用</span>';
			}
		}
		function reloadGrid(){
			$("#grid-table").trigger("reloadGrid"); //重新载入
		}
   </script>
   		
	</body>
</html>
