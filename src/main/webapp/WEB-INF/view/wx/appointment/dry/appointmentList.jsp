<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<jsp:include page="../css.jsp" flush="true" />
</head>

<body>
<div class="layer"></div>
	<div class="main pFooter">
		<div class="orderWrap">
			<ul class="typeNav">
			<c:choose>
				<c:when test="${empty appointType or appointType ==0}">
					<li class="current">未完成</li>
					<li onclick="loadUrl(1)">进行中</li>
					<li onclick="loadUrl(2)">已完成</li>
				</c:when>
				<c:when test="${empty appointType or appointType ==1}">
					<li onclick="loadUrl(0)">未完成</li>
					<li class="current">进行中</li>
					<li onclick="loadUrl(2)">已完成</li>
				</c:when>
				<c:when test="${empty appointType or appointType ==2}">
					<li onclick="loadUrl(0)">未完成</li>
					<li onclick="loadUrl(1)">进行中</li>
					<li class="current">已完成</li>
				</c:when>
			</c:choose>
			</ul>
			<div class="orderBody">
				<ul class="notFinished orderContent changeCt">
				
					<c:forEach items="${memberAppoints }" var="memberAppoint">
						<li>
							<div class="orderItem">
								<h2 class="orderNum rArrow"><a href="/order/orderDetail/${memberAppoint.id }">订单编号：
								${memberAppoint.id }<c:if test="${memberAppoint.appointType==0}">(预约下单)</c:if> </a></h2>
								<div class="itemContent">
									<div class="takeTime">
	
										<p class="title">取件时间</p>
										<p>${memberAppoint.appointTime } </p>
									</div>
									<div class="userInfo">
										<p class="title">
										<span>联系人信息：</span>${memberAppoint.username }</p>
										<p><span>${memberAppoint.mobile }</span>
										<span>${memberAppoint.address }</span></p>
										<p></p>
									</div>
									<div class="orderState">
										<p class="title">支付状态 
											<span class="status">
												<c:choose>
													<c:when test="${memberAppoint.payType == 0 }">
														待付款
													</c:when>
													<c:when test="${memberAppoint.payType == 1 }">
														微信支付
													</c:when>
													<c:when test="${memberAppoint.payType == 2 }">
														线下支付
													</c:when>
												</c:choose>
											</span>
										</p>
									</div>
								</div>
							</div>
							<c:choose>
								<c:when test="${memberAppoint.status == 0 }">
									<ul class="footerBtnWrap orderItemFooter notPay">
										<li onclick="cancelOrder('${memberAppoint.id}')">取消订单</li>
										<li class="payBtn" onclick="topay('${memberAppoint.id}','${memberAppoint.appointType }','${memberAppoint.nowPrice }')">立即付款</li>
									</ul>
								</c:when>
								<c:when test="${memberAppoint.status == 1 }">
									<ul class="footerBtnWrap orderItemFooter notPay">
										<p>已付款：<i class="RMB">￥</i>${memberAppoint.nowPrice}</p>
									</ul>
								</c:when>
							</c:choose>
							<!-- <ul class="footerBtnWrap orderItemFooter payed">
								<li>催单</li>
	
							</ul> -->
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>

	</div>
	<jsp:include page="../js.jsp" flush="true" />
	<script type="text/javascript" src="${res_url}js/appointment/appointment.js"></script>
</body>
<script type="text/javascript">
	function loadUrl(type){
		window.location.href="/appointment/list/"+type;
	}
</script>

</html>