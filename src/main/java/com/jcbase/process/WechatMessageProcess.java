package com.jcbase.process;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jcbase.core.BaseConstance;
import com.jcbase.core.plugin.process.RedisProcesser;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.WeiXinUtils;
import com.jcbase.core.weixin.InMsgParser;
import com.jcbase.core.weixin.service.FaceService;
import com.jcbase.model.RisebdWechatReply;
import com.jcbase.process.adapter.WechatMessageAdapter;
import com.jcbase.service.CompleteService;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.jfinal.weixin.sdk.api.CustomServiceApi;
import com.jfinal.weixin.sdk.msg.in.InImageMsg;
import com.jfinal.weixin.sdk.msg.in.InLinkMsg;
import com.jfinal.weixin.sdk.msg.in.InLocationMsg;
import com.jfinal.weixin.sdk.msg.in.InMsg;
import com.jfinal.weixin.sdk.msg.in.InShortVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.InVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InVoiceMsg;
import com.jfinal.weixin.sdk.msg.in.event.InCustomEvent;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InLocationEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMassEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.in.event.InPoiCheckNotifyEvent;
import com.jfinal.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InShakearoundUserShakeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InTemplateMsgEvent;
import com.jfinal.weixin.sdk.msg.in.event.InVerifyFailEvent;
import com.jfinal.weixin.sdk.msg.in.event.InVerifySuccessEvent;
import com.jfinal.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;

/**
 * @category 微信服务队列处理
 * 
 * @author yiz
 * @date 2016年7月28日 下午2:02:31
 * @version 1.0.0
 * @copyright pycredit.cn
 */
public class WechatMessageProcess extends WechatMessageAdapter implements RedisProcesser {
	private static final Logger logger = LoggerFactory.getLogger(WechatMessageProcess.class);

	/**
	 * @category 必须实现此方法
	 */
	@Override
	public void doProcess(String queueName, String content) {
		if (CommonUtils.isNotEmpty(queueName) && BaseConstance.WX_RESPONSE.equals(queueName)
				&& CommonUtils.isNotEmpty(content)) {
			processWechatQueue(content);
		}
	}

	private void processWechatQueue(String content) {
		try {
			// 微信消息队列时
			logger.info("得到的微信消息为content = {}", content);
			JSONObject jsonObject = JSON.parseObject(content);
			String ToUserName = jsonObject.getString("ToUserName");
			logger.info("ToUserName={}", ToUserName);
			ApiConfigKit.setThreadLocalApiConfig(WeiXinUtils.getApiConfig(null));
			// 转JSON对象
			// 消息转对象
			InMsg msg = InMsgParser.doParse(jsonObject);
			if (msg instanceof InTextMsg)
				processInTextMsg((InTextMsg) msg);
			else if (msg instanceof InImageMsg)
				processInImageMsg((InImageMsg) msg);
			else if (msg instanceof InVoiceMsg)
				processInVoiceMsg((InVoiceMsg) msg);
			else if (msg instanceof InVideoMsg)
				processInVideoMsg((InVideoMsg) msg);
			else if (msg instanceof InShortVideoMsg) // 支持小视频
				processInShortVideoMsg((InShortVideoMsg) msg);
			else if (msg instanceof InLocationMsg)
				processInLocationMsg((InLocationMsg) msg);
			else if (msg instanceof InLinkMsg)
				processInLinkMsg((InLinkMsg) msg);
			else if (msg instanceof InCustomEvent)
				processInCustomEvent((InCustomEvent) msg);
			else if (msg instanceof InFollowEvent)
				processInFollowEvent((InFollowEvent) msg);
			else if (msg instanceof InQrCodeEvent)
				processInQrCodeEvent((InQrCodeEvent) msg);
			else if (msg instanceof InLocationEvent)
				processInLocationEvent((InLocationEvent) msg);
			else if (msg instanceof InMassEvent)
				processInMassEvent((InMassEvent) msg);
			else if (msg instanceof InMenuEvent)
				processInMenuEvent((InMenuEvent) msg);
			else if (msg instanceof InSpeechRecognitionResults)
				processInSpeechRecognitionResults((InSpeechRecognitionResults) msg);
			else if (msg instanceof InTemplateMsgEvent)
				processInTemplateMsgEvent((InTemplateMsgEvent) msg);
			else if (msg instanceof InShakearoundUserShakeEvent)
				processInShakearoundUserShakeEvent((InShakearoundUserShakeEvent) msg);
			else if (msg instanceof InVerifySuccessEvent)
				processInVerifySuccessEvent((InVerifySuccessEvent) msg);
			else if (msg instanceof InVerifyFailEvent)
				processInVerifyFailEvent((InVerifyFailEvent) msg);
			else if (msg instanceof InPoiCheckNotifyEvent)
				processInPoiCheckNotifyEvent((InPoiCheckNotifyEvent) msg);
		} finally {
			ApiConfigKit.removeThreadLocalApiConfig();
		}
	}

	@Override
	protected void processInFollowEvent(InFollowEvent inFollowEvent) {
		if (InFollowEvent.EVENT_INFOLLOW_SUBSCRIBE.equals(inFollowEvent.getEvent())) {
			String openid = inFollowEvent.getFromUserName();
			logger.debug("关注：" + openid);
			//获取用户信息判断是否关注
			CompleteService.followerSubscribe(openid);
			List<RisebdWechatReply> risebdWechatReplies = RisebdWechatReply.me.getByTypeAndKey(1, null);
			if (null != risebdWechatReplies && risebdWechatReplies.size() > 0) {
				RisebdWechatReply risebdWechatReply = risebdWechatReplies.get(0);
				//发送微信客服消息
				CompleteService.sendMsg(openid, risebdWechatReply);
			}
		}
		// 如果为取消关注事件，将无法接收到传回的信息
		if (InFollowEvent.EVENT_INFOLLOW_UNSUBSCRIBE.equals(inFollowEvent.getEvent())) {
			logger.debug("取消关注：" + inFollowEvent.getFromUserName());
			CompleteService.updateSubscribe(inFollowEvent.getFromUserName());
		}
	}

	@Override
	protected void processInTextMsg(InTextMsg inTextMsg) {
		String content = inTextMsg.getContent();
		if (CommonUtils.isNotEmpty(content)) {
			logger.info("开始回复...content={}", content);
			// 判断后台设置
			List<RisebdWechatReply> risebdWechatReplies = RisebdWechatReply.me.getByTypeAndKey(2, content);
			if (null != risebdWechatReplies && risebdWechatReplies.size() > 0) {
				RisebdWechatReply risebdWechatReply = risebdWechatReplies.get(0);
				//发送微信客服消息
				CompleteService.sendMsg(inTextMsg.getFromUserName(), risebdWechatReply);
			}
		}
	}

	@Override
	protected void processInMenuEvent(InMenuEvent inMenuEvent) {
		String event = inMenuEvent.getEvent();
		logger.info("事件event={},key={}", event,inMenuEvent.getEventKey());
		List<RisebdWechatReply> risebdWechatReplies = RisebdWechatReply.me.getByTypeAndKey(3, inMenuEvent.getEventKey());
		if (null != risebdWechatReplies && risebdWechatReplies.size() > 0) {
			RisebdWechatReply risebdWechatReply = risebdWechatReplies.get(0);
			//发送微信客服消息
			CompleteService.sendMsg(inMenuEvent.getFromUserName(), risebdWechatReply);
		}
	}

	@Override
	protected void processInImageMsg(InImageMsg inImageMsg) {
		logger.info("图片处理。。。");
		String PicUrl = inImageMsg.getPicUrl();//得到为图片链接
		String result = FaceService.detect(PicUrl);
		CustomServiceApi.sendText(inImageMsg.getFromUserName(), result);
	}

}
