package com.jcbase.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jcbase.core.auth.interceptor.AuthorityInterceptor;
import com.jcbase.core.auth.interceptor.SysLogInterceptor;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.util.DESUtils;
import com.jfinal.aop.Clear;
import com.jfinal.kit.PropKit;

/**
 * @category 部分通用
 * 
 * @author yiz
 * @date 2016年7月28日 上午9:27:29
 * @version 1.0.0 
 * @copyright pycredit.cn 
 */
@Clear({ AuthorityInterceptor.class, SysLogInterceptor.class })
public class ExtController extends JCBaseController {

	/**
	 * @category 解密
	 */
	public void desDecrypt() {
		String para = getPara("para");
		String decryptResult = new DESUtils(PropKit.get("des_key")).decryptString(para);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("decryptResult", decryptResult);
		renderJson(JSON.toJSONString(jsonObject));
	}

	/**
	 * @category 加密
	 */
	public void desEncrypt() {
		String para = getPara("para");
		String encryptResult = new DESUtils(PropKit.get("des_key")).encryptString(para);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("encryptResult", encryptResult);
		renderJson(JSON.toJSONString(jsonObject));
	}
}
