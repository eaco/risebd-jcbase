<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script src="${res_url}js/router.min.js"></script>
<script src="${res_url}js/zepto.min.js"></script>
<script src="${res_url}js/appointment/common.js"></script>
<script src="${res_url}js/banner.js"></script>
<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='${res_url}ace-1.3.3/assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${res_url}ace-1.3.3/assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
<script src="${res_url}js/layer/layer.js"></script>