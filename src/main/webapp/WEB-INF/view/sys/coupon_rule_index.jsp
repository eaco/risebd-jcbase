<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Risebd后台管理系统</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- bootstrap & fontawesome -->
		<jsp:include page="/WEB-INF/view/common/basecss.jsp" flush="true" />
	</head>
	<body class="no-skin">
		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
		<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			<div class="main-content" id="page-wrapper">
				<div class="page-content" id="page-content">
					<div class="row">
							<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<div class="widget-box">
											<div class="widget-header widget-header-small">
												<h5 class="widget-title lighter">筛选</h5>
												<span style="padding-left: 92%"><a class="btn btn-success radius r" style="margin-top: 3px; margin-bottom: 3px; line-height: 1.2em;" href="javascript:location.replace(location.href);" title="刷新"><i class="fa fa-refresh"></i></a></span>
											</div>
											<div class="widget-body">
												<div class="widget-main">
														<div class="row">
															<div class="col-xs-12 col-sm-8">
																<div class="row-fluid" style="margin-bottom: 5px;">
																	<div class="span12 control-group">
																		<jc:button className="btn btn-primary" id="bnt-add" textName="添加" permission="/sys/couponRule/save"/>
																		<jc:button className="btn btn-info" id="bnt-edit" textName="编辑" permission="/sys/couponRule/save" />
																		<jc:button className="btn btn-delete" id="bnt-del" textName="删除" permission="/sys/couponRule/delete"/>
																		<jc:button className="btn btn-success" id="bnt-create" textName="生成优惠券" permission="/sys/couponRule/save"/>
																		<jc:button className="btn btn-delete" id="bnt-rollback" textName="优惠券强制收回" permission="/sys/couponRule/rollback"/>
																	</div>
																</div>
																<div class="input-group">
																	<span class="input-group-addon">
																		<i class="ace-icon fa fa-check"></i>
																	</span>

																	<input type="text" id="title" name="title" class="form-control search-query" placeholder="请输入关键字" />
																	<span class="input-group-btn">
																		<button type="button" id="btn_search" class="btn btn-purple btn-sm">
																			<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																			搜索
																		</button>
																	</span>
																	
																</div>
															</div>
														</div>
												</div>
											</div>
										</div>
							</div>
							<div class="col-xs-12">
								<div class="row-fluid" style="margin-bottom: 5px;">
									
								</div>
								<!-- PAGE CONTENT BEGINS -->
								<table id="grid-table"></table>

								<div id="grid-pager"></div>

								<script type="text/javascript">
									var $path_base = "..";//in Ace demo this will be used for editurl parameter
								</script>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div>	
				</div>
			</div>
		</div><!-- /.main-container -->
		<!-- basic scripts -->
		<jsp:include page="/WEB-INF/view/common/basejs.jsp" flush="true" />
			<script type="text/javascript"> 
			function format_type(cellvalue, options, rowObject){
				if(1==cellvalue){
					return "新用户注册奖励";
				}else if(2==cellvalue){
					return "推广奖励";
				}else if(3==cellvalue){
					return "被推广奖励";
				}else if(4==cellvalue){
					return "活动自动赠送";
				}else if(5==cellvalue){
					return "活动领取";
				}else if(6==cellvalue){
					return "用于用户兑换";
				}
				return cellvalue;
			}
			function format_status(cellvalue, options, rowObject){
				if(0==cellvalue){
					return "已生成规则";
				}else if(1==cellvalue){
					return "已完成优惠券的生成";
				}else if(2==cellvalue){
					return "可领取的优惠券";
				}else if(3==cellvalue){
					return "超时不可领取"
				}else if(4==cellvalue){
					return "已被禁止"
				}else if(5==cellvalue){
					return "已被删除"
				}
				return cellvalue;
			}
			function format_form(cellvalue, options, rowObject){
				if(1==cellvalue){
					return "抵扣券";
				}else if(2==cellvalue){
					return "折扣券";
				}
			}
			var selectRowid=-1;
        $(document).ready(function () {
        	var grid_selector = "#grid-table";
			var pager_selector = "#grid-pager";
        	//resize to fit page size
			$(window).on('resize.jqGrid', function () {
				$(grid_selector).jqGrid( 'setGridWidth', $(".page-content").width() );
		    });
//resize on sidebar collapse/expand
				var parent_column = $(grid_selector).closest('[class*="col-"]');
				$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
					if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
						//setTimeout is for webkit only to give time for DOM changes and then redraw!!!
						setTimeout(function() {
							$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
						}, 0);
					}
			    });

            $("#grid-table").jqGrid({
                //url: 'http://trirand.com/blog/phpjqgrid/examples/jsonp/getjsonp.php?callback=?&qwery=longorders',
                url:'/sys/couponRule/getListData',
                mtype: "GET",
                datatype: "json",
                colModel: [
					{ label: 'ID', name: 'id', key: true,hidden:true},
                    { label: '标题', name: 'title', width: 90,sortable:false },
                    { label: '类型', name: 'type',width: 75,formatter:format_type },
                    { label: '折扣形式', name: 'conpon_form',width: 50,formatter:format_form },
                    { label: '优惠力度', name: 'coupon_amount',width: 50,sortable:false },
                    { label: '优惠券数量', name: 'coupon_num',width: 50,sortable:false },
                    { label: '已领取数量', name: 'receive_num',width: 50,sortable:false },
                    { label: '每人限量(次)', name: 'get_times',width: 50,sortable:false  },
                    { label: '订单最小金额', name: 'limit_amount',width: 50,sortable:false },
                    { label: '优惠券编号', name: 'coupon_no',width: 80,sortable:false },
                    { label: '状态', name: 'status',width: 80,sortable:false,formatter:format_status },
                    { label: '定时发送时间', name: 'start_timing',width: 100,sortable:false },
                    { label: '有效开始时间', name: 'coupon_start_time',width: 100,sortable:false },
                    { label: '有效结束时间', name: 'coupon_end_time',width: 100,sortable:false },
                ],
				viewrecords: true,
                height: 560,
                rowNum: 20,
                sortname:"id",
                sortorder:"desc",
                altRows: true,//隔行变色
                recordtext:"{0} - {1} 共 {2} 条",
                pgtext:"第 {0} 页 共 {1} 页",
                pager:pager_selector,
				onSelectRow : function( rowid ) {
					if(rowid) 
					{
						selectRowid=rowid;
						/**if(rdata.isLeaf === 'true') {
							$("#price").html(rdata.price);
							$("#uiicon").empty().append("<span class='ui-icon "+rdata.uiicon+"'></span>");
						}**/
					} 
				},
                loadComplete : function() {
					var table = this;
					setTimeout(function(){
						updatePagerIcons(table);
					}, 0);
				}
            });
			$(window).triggerHandler('resize.jqGrid');
			
			$("#bnt-rollback").click(function(){
				var rdata=getSelectedRows();
				var id=rdata.id;
				if (typeof(id) == "undefined") { 
					layer.msg("请选择要强制回收的优惠券规则!");
					return;
				}
				$.post("/sys/couponRule/getStatus?id="+id,function(data){
					if(data.status!=0 && data.status !=4){
						layer.confirm('确定要强制停止优惠券规则？停止后不可恢复', {
							  btn: ['我已经肯定了!','容我考虑下~'] //按钮
							}, function(){
								$.post("/sys/couponRule/rollback" ,{"id":id,"status":4},function(data){
					        		if(data.code=='success'){
					        			layer.msg('操作成功', {
					        				icon: 1,
					        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
					        			},function(){
					        				 $("#grid-table").trigger("reloadGrid"); //重新载入
					        			});
					        		}else{
					        			layer.msg('操作失败', {
					        				icon: 2,
					        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
					        			},function(){
					        				 $("#grid-table").trigger("reloadGrid"); //重新载入
					        			});
					        		}
					        		$("#submit-btn").removeClass("disabled");
					        	},"json");
							}, function(){
								  
							});
					}else{
						layer.msg('该状态无需回收!', {
	        				icon: 2,
	        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
	        			}); 	
					}
				});
			});
			
			$("#btn_search").click(function(){  
			    //此处可以添加对查询数据的合法验证  
			    var title = $("#title").val();  
			    $("#grid-table").jqGrid('setGridParam',{  
			        datatype:'json',  
			        postData:{'title':title}, //发送数据  
			        page:1  
			    }).trigger("reloadGrid"); //重新载入  
			}); 
			
			$("#bnt-add").click(function(){
				parent.layer.open({
				    type: 2,
				   // shade: [1],
				    fix: false,
				    title: '添加优惠券规则',
				    maxmin: true,
				   	content: '/sys/couponRule/add',
				    area: ['920px', '100%']
				}); 
			});
			
			$("#bnt-edit").click(function(){
				var rdata=getSelectedRows();
				var id=rdata.id;
				if (typeof(id) == "undefined") { 
					layer.msg("请选择要编辑的优惠券规则");
					return;
				}  
				$.post("/sys/couponRule/getStatus?id="+id,function(data){
					if(data.status==0){
						parent.layer.open({
						    type: 2,
						   // shade: [1],
						    fix: false,
						    title: '编辑优惠券规则',
						    maxmin: true,
						   	content: '/sys/couponRule/add?id='+id,
						    area: ['920px', '100%']
						}); 
					}else{
						layer.msg('该状态不可编辑!', {
	        				icon: 2,
	        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
	        			}); 	
					}
				});
			});
			
			$("#bnt-del").click(function(){
				var rdata=getSelectedRows();
				var id=rdata.id;
				if (typeof(id) == "undefined") { 
					layer.msg("请选择要删除的优惠券规则");
					return;
				}  
				$.post("/sys/couponRule/getStatus?id="+id,function(data){
					if(data.status==0 || data.status ==4){
						layer.confirm('确定要删除此列？删除后不可恢复', {
							  btn: ['我已经肯定了!','容我考虑下~'] //按钮
							}, function(){
								$.post("/sys/couponRule/updateStatus" ,{"id":id,"status":5},function(data){
					        		if(data.code=='success'){
					        			layer.msg('操作成功', {
					        				icon: 1,
					        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
					        			},function(){
					        				 $("#grid-table").trigger("reloadGrid"); //重新载入
					        			});
					        		}else{
					        			layer.msg('操作失败', {
					        				icon: 2,
					        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
					        			},function(){
					        				 $("#grid-table").trigger("reloadGrid"); //重新载入
					        			});
					        		}
					        		$("#submit-btn").removeClass("disabled");
					        	},"json");
							}, function(){
								  
							});
					}else{
						layer.msg('该状态不可删除!', {
	        				icon: 2,
	        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
	        			}); 	
					}
				});
			});
			
			$("#bnt-create").click(function(){
				var rdata=getSelectedRows();
				var id=rdata.id;
				if (typeof(id) == "undefined") { 
					layer.msg("请选择要生成优惠券的规则");
					return;
				}  
				layer.confirm('确定开始生成优惠券？生成后可直接领取或者开始发放!', {
					  btn: ['我已经肯定了!','容我考虑下~'] //按钮
					}, function(){
						$.post("/sys/couponRule/create?id="+id,function(data){
			        		if(data.errcode==0){
			        			layer.msg(data.msg, {
			        				icon: 1,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			},function(){
			        				 $("#grid-table").trigger("reloadGrid"); //重新载入
			        			});
			        			
			        		}else{
			        			layer.msg(data.msg, {
			        				icon: 2,
			        			    time: 1000 //2秒关闭（如果不配置，默认是3秒）
			        			});
			        		}
			        	},"json");
					}, function(){
						  
					});
			});
			
        });
      //replace icons with FontAwesome icons like above
		function updatePagerIcons(table) {
			var replacement = 
			{
				'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
				'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
				'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
				'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
			};
			$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
			})
		}
		/**获取选中的列***/
		function getSelectedRows() {
			var rdata = $('#grid-table').jqGrid('getRowData', selectRowid);
			return rdata;      
        }
		function setVisible(status){
			alert(getSelectedRows());
		}
		//格式化状态显示
		function fmatterStatus(cellvalue, options, rowObject){
			if(cellvalue==0){
				return '<span class="label label-sm label-warning">禁用</span>';
			}else{
				return '<span class="label label-sm label-success">启用</span>';
			}
		}
		function reloadGrid(){
			$("#grid-table").trigger("reloadGrid"); //重新载入
		}
   </script>
   		
	</body>
</html>
