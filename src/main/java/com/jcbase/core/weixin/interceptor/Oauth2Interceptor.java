package com.jcbase.core.weixin.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcbase.core.BaseConstance;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.WeiXinUtils;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.PropKit;
import com.jfinal.weixin.sdk.api.ApiConfigKit;

/**
 * @category 授权
 * 
 * @author yiz
 * @date 2016年8月8日 下午4:17:41
 * @version 1.0.0 
 * @copyright 
 */
public class Oauth2Interceptor implements Interceptor {
	private static final Logger logger = LoggerFactory.getLogger(Oauth2Interceptor.class);

	@Override
	public void intercept(Invocation inv) {
		logger.info("OAuth拦截器开始");
		Controller controller = inv.getController();
		String toUserName = controller.getPara("toUserName");
		//是否允许非微信端访问
		boolean allowNoWechat = PropKit.getBoolean("allowNoWechat", false);
		try {
			ApiConfigKit.setThreadLocalApiConfig(WeiXinUtils.getApiConfig(toUserName));
			String openid = controller.getCookie(BaseConstance.OPENID_COOKIE_KEY);
			String memberid = controller.getCookie(BaseConstance.MEMBERID_COOKIE_KEY);
			logger.info("openid={}", openid);
			if (CommonUtils.isEmpty(openid) && !allowNoWechat) {
				//重定向至授权页面并返回
				controller.redirect("/wx/toOauth?redirect_url=" + BaseConstance.DOMAIN);
			}
			/*else if (allowNoWechat && CommonUtils.isEmpty(memberid)) {//当会员为空时，且允许非微信端访问
				//重定向登陆页面
				controller.redirect("/wap/login");
			} */
			else {
				controller.setCookie(BaseConstance.OPENID_COOKIE_KEY, openid, 30 * 3600);
				inv.invoke();
			}
		} finally {
			ApiConfigKit.removeThreadLocalApiConfig();
		}
	}

}
