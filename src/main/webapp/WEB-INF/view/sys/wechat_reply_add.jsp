<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<jsp:include page="/WEB-INF/view/common/basecss.jsp" flush="true" />
</head>

<body class="no-skin">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<div class="main-content" style="margin-left: 0px;">
					<div class="page-content">
						<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<form class="form-horizontal" id="validation-form" method="post">
													<div class="form-group">
														<input name="id" type="hidden" value="${wechatReply.id}"/>
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">关键字/事件key</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
															
													            <input <c:if test="${!empty wechatReply.keyword }">readonly</c:if> type="text" name="keyword" id="keyword" value="${wechatReply.keyword}" class="col-xs-12 col-sm-6"  />
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="type">类型</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="msg_type" id="msg_type" class="col-xs-12 col-sm-6">
																	<option value="1">文本</option>
																	<option value="2">图片</option>
																	<option value="3">图文</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="type">类型</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="type" id="type" class="col-xs-12 col-sm-6">
																	<option value="1">欢迎回复</option>
																	<option value="2">关键字内容回复</option>
																	<option value="3">事件回复</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="title">标题(图文时必填)</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="title" id="title" value="${wechatReply.title}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">跳转链接</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="url" id="url" value="${wechatReply.url}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="author">封面</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input id="fileupload" type="file" name="files[]" data-url="/fileupload/uploadImg"  multiple >
																<img alt="暂无图片" src="${ static_url}${wechatReply.mainImg }" id="img" width="50px" onerror="javascript:this.src='${pageContext.request.contextPath}/res/img/default.jpg'">
													            <input type="hidden" name="main_img" id="main_img" value="${wechatReply.mainImg}" >
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">内容</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <textarea rows="5" cols="31" name="content" id="content">${wechatReply.content }</textarea>
															</div>
														</div>
													</div>
													
													<div class="clearfix form-actions" align="center">
														<div class="col-md-offset-3 col-md-9">
															<button id="submit-btn" class="btn btn-info" type="submit" data-last="Finish">
																<i class="ace-icon fa fa-check bigger-110"></i>
																提交
															</button>
															&nbsp; &nbsp; &nbsp;
															<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												重置
											</button>
														</div>
													</div>
												</form>
						</div><!-- /.col -->
					</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
		</div><!-- /.main-container -->
			<!-- basic scripts -->
<jsp:include page="/WEB-INF/view/common/basejs.jsp" flush="true" />
<script src="/res/js/ztree/js/jquery.ztree.core-3.5.min.js"></script>
	<script type="text/javascript">
			function defaultSelect(id,value){
				if(value){
					//默认选中
					$("#"+id).val(value);
				}
			}
			$(document).ready(function(){
				defaultSelect("type","${wechatReply.type}");
				defaultSelect("msg_type","${wechatReply.msgType}");
				
				initformSubmitEvent();
				$('#fileupload').fileupload({
				    done: function (e, data) {//设置文件上传完毕事件的回调函数  
				    	var result = data.result;
				    	if(result.code==0){
				    		layer.alert("上传文件失败", 9, !1);
				    		return false;
				    	}else if(result.code==1){
				    		$("#img").attr("src",result.staticUrl+result.fileUrl);
				    		$("#main_img").val(result.fileUrl);
				    	}
				    }
				});
			});
			
			var setting = {
			view: {
				dblClickExpand: false
			},
			data: {
				simpleData: {
				enable: true
			}
			},
			callback: {
				beforeClick: beforeClick,
				onClick: onClick
			}
			};
			
			var $validation = true;
			function initformSubmitEvent(){
				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					rules: {
						content:{
							required: true
						},
						type:{
							required: true
						},
						msg_type:{
							required: true
						},
						url:{
							url:true
						},
					},
					messages: {
						content:{
							required: "请输入内容"
						},
						type:{
							required: "请选择类型"
						},
						url:{
							url:"请输入正确的链接地址"
						},
						msg_type:{
							required: "请选择消息类型"
						},
					},
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
						var $form = $("#validation-form");
						var $btn = $("#submit-btn");
						if($btn.hasClass("disabled")) return;
						$btn.addClass("disabled");
						 var postData=$("#validation-form").serialize();
						 var keywordValue= $("#keyword").val();
						 var type = $("#type").val();
						 if(type=='1'){
							 if(keywordValue){
								 layer.msg('欢迎词不可设置关键字或事件key', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			}); 
								 $btn.removeClass("disabled");
								 return;
							 }
						 }
						 var msg_type = $("#msg_type").val();
						 if(msg_type=='1'){
							 if($("#main_img").val()){
								 layer.msg('文本无需上传图片', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			}); 
								 $btn.removeClass("disabled");
								 return;
							 }
							 if($("#url").val()){
								 layer.msg('文本无需填写链接', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			}); 
								 $btn.removeClass("disabled");
								 return;
							 }
						 }
						 if(msg_type=='2' || msg_type=='3'){
							 if($("#main_img").val()=='' || $("#main_img").val()==null){
								 layer.msg('请上传图片', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			}); 
								 $btn.removeClass("disabled");
								 return;
							 }
							 if(msg_type=='2'){
								 if($("#url").val()){
									 layer.msg('图片无需填写链接', {
					        				icon: 2,
					        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
					        			}); 
									 $btn.removeClass("disabled");
									 return;
								 }
							 }
							 if(msg_type=='3'){
								 if(!$("#title").val()){
									 layer.msg('请输入标题', {
					        				icon: 2,
					        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
					        			}); 
									 $btn.removeClass("disabled");
									 return;
								 }
							 }
						 }
			        	 $.post("/sys/wechat/reply/saveRes" ,postData,function(data){
			        		if(data.code=='success'){
			        			layer.msg('操作成功', {
			        				icon: 1,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			},function(){
			        				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			        				parent.reloadGrid();
			        				parent.layer.close(index); //再执行关闭 
			        			});
			        		}else{
			        			layer.msg('操作失败，不可以有重复的关键字，事件key或欢迎词', {
			        				icon: 2,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			});
			        		}
			        		$btn.removeClass("disabled");
			        	},"json");
						return false;
					},
					invalidHandler: function (form) {
					}
				});
			}
		</script>
</body>

</html>

