<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<jsp:include page="/WEB-INF/view/common/basecss.jsp" flush="true" />
</head>

<body class="no-skin">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<div class="main-content" style="margin-left: 0px;">
					<div class="page-content">
						<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<form class="form-horizontal" id="validation-form" method="post">
								<div class="form-group">
									<input name="id" type="hidden" value="${productType.id}"/>
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="title">名称</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
								            <input type="text" name="name" id="name" value="${productType.name}" class="col-xs-12 col-sm-6">
										</div>
									</div>
								</div>
								<c:if test="${ productTypes.size()>0 }">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">父级类型</label>
										<div class="col-xs-12 col-sm-9">
											<div class="clearfix">
									            <select name="parent_type" id="parent_type" <c:if test="${!empty productType }">disabled</c:if> class="col-xs-12 col-sm-6"  >
									            	<option value="">请选择</option>
									            	<c:forEach items="${productTypes }" var="productTypeP">
														<option value="${productTypeP.type }">${productTypeP.name }</option>
									            	</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</c:if>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="url">类型ID(数字)</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
											 <input type="text" <c:if test="${!empty productType.type }">readonly</c:if> name="type" id="type" value="${productType.type}" class="col-xs-12 col-sm-6">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">类型等级</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
								            <select name="type_level" id="type_level" <c:if test="${!empty productType }">disabled</c:if> class="col-xs-12 col-sm-6"  >
												<option value="1">主类型</option>
												<option value="2">次类型</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="author">封面</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
											<input id="fileupload" type="file" name="files[]" data-url="/fileupload/uploadImg"  multiple >
											<img alt="暂无图片" src="${ static_url}${productType.mainImg }" id="img" width="50px" onerror="javascript:this.src='${pageContext.request.contextPath}/res/img/default.jpg'" >
								            <input type="hidden" name="main_img" id="main_img" value="${productType.mainImg}" >
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">描述</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
								            <textarea rows="5" cols="31" name="description" id="description">${productType.description }</textarea>
										</div>
									</div>
								</div>
								<div class="clearfix form-actions" align="center">
									<div class="col-md-offset-3 col-md-9">
										<button id="submit-btn" class="btn btn-info" type="submit" data-last="Finish">
											<i class="ace-icon fa fa-check bigger-110"></i>
											提交
										</button>
										&nbsp; &nbsp; &nbsp;
										<button class="btn" type="reset">
											<i class="ace-icon fa fa-undo bigger-110"></i>
											重置
										</button>
									</div>
								</div>
							</form>
						</div><!-- /.col -->
					</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
		</div><!-- /.main-container -->
			<!-- basic scripts -->
<jsp:include page="/WEB-INF/view/common/basejs.jsp" flush="true" />

	<script type="text/javascript">
			$(document).ready(function(){
				initformSubmitEvent();
				defaultSelect("type_level","${productType.typeLevel}");
				defaultSelect("parent_type","${productType.parentType}");
				$('#fileupload').fileupload({
				    done: function (e, data) {//设置文件上传完毕事件的回调函数  
				    	var result = data.result;
				    	if(result.code==0){
				    		layer.alert("上传文件失败", 9, !1);
				    		return false;
				    	}else if(result.code==1){
				    		$("#img").attr("src",result.staticUrl+result.fileUrl);
				    		$("#main_img").val(result.fileUrl);
				    	}
				    }
				});
			});
			var setting = {
			view: {
				dblClickExpand: false
			},
			data: {
				simpleData: {
				enable: true
			}
			},
			callback: {
				beforeClick: beforeClick,
				onClick: onClick
			}
			};
			
			//json数据源，也可以从后台读取json字符串，并转换成json对象，如下所示
			var strNodes = '${jsonTree}';
			var zNodes = eval("("+strNodes+")"); //将json字符串转换成json对象数组，strNode一定要加"（）"，不然转不成功
			function beforeClick(treeId, treeNode) {
				var check = (treeNode.id!=10000);
			if (!check) alert("这是标题，不能选！");
				return check;
			}
			function onClick(e, treeId, treeNode) {
				$("#parent_id").val(treeNode.id);
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getSelectedNodes(),
				v = "";
				nodes.sort(function compare(a,b){return a.id-b.id;});
			for (var i=0, l=nodes.length; i<l; i++) {
				v += nodes[i].name + ",";
			}
				if (v.length > 0 ) v = v.substring(0, v.length-1);
				var cityObj = $("#parent_name");
				cityObj.attr("value", v); 
			}
			
			function showMenu() {
				var cityObj = $("#parent_name");
				var cityOffset = $("#parent_name").offset();
				var sel=document.getElementById("kka");
				$("#menuContent").css({left:sel.offsetLeft + "px", top:sel.offsetTop + cityObj.outerHeight() + "px"}).slideDown("fast");
				$("body").bind("mousedown", onBodyDown);
			}
			function hideMenu() {
				$("#menuContent").fadeOut("fast");
				$("body").unbind("mousedown", onBodyDown);
			}
			function onBodyDown(event) {
				if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
					hideMenu();
				}
				if(event.target.id.length>=15&&event.target.id.length!=17){
					hideMenu();
				}
			}
			function defaultSelect(id,value){
				//默认选中
				if(value){
					$("#"+id).val(value);
				}
			}
			
			var $validation = true;
			function initformSubmitEvent(){
				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					rules: {
						name:{
							required: true
						},
						type_level:{
							required: true
						},
						type:{
							required: true,
							digits:true
						}
					},
					messages: {
						name:{
							required: "请输入名称"
						},
						type_level:{
							required: "请选择类型等级"
						},
						type:{
							required: "请输入类型ID",
							digits:"类型只能输入整数"
						}
					},
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
						var $form = $("#validation-form");
						var $btn = $("#submit-btn");
						debugger;
						var parent_typeValue = $("#parent_type").val();
						var type_levelValue = $("#type_level").val();
						if(type_levelValue=='1' && parent_typeValue){
							layer.msg('主类不可有父级类型', {
		        				icon: 2,
		        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
		        			});
							 $btn.removeClass("disabled");
							return;
						}
						if(type_levelValue=='2' && !parent_typeValue){
							layer.msg('次类必须有父级类型', {
		        				icon: 2,
		        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
		        			});
							 $btn.removeClass("disabled");
							return;
						}
						if($btn.hasClass("disabled")) return;
						$btn.addClass("disabled");
						 var postData=$("#validation-form").serialize();
			        	 $.post("/sys/productType/save" ,postData,function(data){
			        		if(data.code=='success'){
			        			layer.msg('操作成功', {
			        				icon: 1,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			},function(){
			        				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			        				parent.reloadGrid();
			        				parent.layer.close(index); //再执行关闭 
			        			});
			        		}else{
			        			layer.msg('不可重复的类型ID', {
			        				icon: 2,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			});
			        		}
			        	},"json");
		        		$("#submit-btn").removeClass("disabled");
						return false;
					},
					invalidHandler: function (form) {
					}
				});
			}
		</script>
</body>

</html>

