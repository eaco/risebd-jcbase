package com.jcbase.controller;

import com.jcbase.core.auth.interceptor.AuthorityInterceptor;
import com.jcbase.core.auth.interceptor.SysLogInterceptor;
import com.jcbase.model.RisebdArticle;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;

/**
 * @category 文章
 * 
 * @author yiz
 * @date 2016年8月22日 下午3:29:27
 * @version 1.0.0 
 */
@Clear({ AuthorityInterceptor.class, SysLogInterceptor.class })
public class ArticleController extends Controller {
	public void view() {
		String id = getPara(0);
		if (id != null) {
			RisebdArticle risebdArticle = RisebdArticle.me.getById(id);
			setAttr("risebdArticle", risebdArticle);
		}
		this.renderJsp("article_view.jsp");
	}

}
