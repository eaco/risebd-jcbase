/**
 * @categery 地址相关
 */
var editMemberAddressUrl = "/uc/doEditAddress";
var delAddrUrl = "/uc/delAddr";
var setAddressDefaultUrl = "/uc/setAddressDefault";
function doEditMemberAddress() {
	var moduleType = $("#moduleType").val();
	var currentCity = $("#currentCity").val();
	var province = $('#loc_province').select2('data').text;
	var city = $('#loc_city').select2('data').text;
	var area = $('#loc_town').select2('data').text;
	
	var k_province = $('#loc_province').val();
	var k_city = $('#loc_city').val();
	var k_area = $('#loc_town').val();
	var username = $("#name").val();
	var mobile = $("#mobile").val();
	var address = $("#address").val();
	if (mobile == '' || mobile.length != 11) {
		$("#mobile").focus();
		layer.msg("请输入正确手机号码", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}
	if (username == '') {
		$("#name").focus();
		layer.msg("请输入姓名", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}
	if (province == '省份') {
		layer.msg("请选择省份", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}
	if (city == '地级市') {
		layer.msg("请选择地级市", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}
	if (area == '市、县、区') {
		layer.msg("请选择市、县、区", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}
	if (address == '') {
		$("#address").focus();
		layer.msg("请输入正确地址，方便我们与您进行联系~", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}
	if ('' != currentCity && currentCity != city) {
		layer.confirm('您当前所在城市与选择城市不一样，是否继续操作', {
			btn : [ '确定了!', '考虑下~' ]
		// 按钮
		}, function() {
			var params = {};
			params['id'] = $("#id").val();
			params['mobile'] = mobile;
			params['username'] = username;
			params['province'] = province;
			params['city'] = city;
			params['area'] = area;
			params['k_province'] = k_province;
			params['k_city'] = k_city;
			params['k_area'] = k_area;
			params['address'] = address;
			params['is_default'] = $("#is_default").val();
			$.post(editMemberAddressUrl, params, function(data) {
				if (data.code == 'success') {
					layer.msg(data.result, {
						icon : 1,
						time : 2000
					// 2秒关闭（如果不配置，默认是3秒）
					});
					if(moduleType == '3'){
						window.location.href = "/appointment/simple";
					}else{
						window.location.href = "/uc/addressList";
					}
				} else {
					layer.msg(data.result, {
						icon : 2,
						time : 2000
					// 2秒关闭（如果不配置，默认是3秒）
					});
				}
			});
		}, function() {
		});
	} else if ('' != currentCity) {
		var params = {};
		params['id'] = $("#id").val();
		params['mobile'] = $("#mobile").val();
		params['username'] = $("#name").val();
		params['province'] = province;
		params['city'] = city;
		params['area'] = area;
		params['k_province'] = k_province;
		params['k_city'] = k_city;
		params['k_area'] = k_area;
		params['address'] = $("#address").val();
		params['is_default'] = $("#is_default").val();
		$.post(editMemberAddressUrl, params, function(data) {
			if (data.code == 'success') {
				layer.msg(data.result, {
					icon : 1,
					time : 2000
				// 2秒关闭（如果不配置，默认是3秒）
				});
				if(moduleType == '3'){
					window.location.href = "/appointment/simple";
				}else{
					window.location.href = "/uc/addressList";
				}
			} else {
				layer.msg(data.result, {
					icon : 2,
					time : 2000
				// 2秒关闭（如果不配置，默认是3秒）
				});
			}
		});
	} else if ('' == currentCity) {
		layer.msg("未选中当前城市", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
	}
}

function delAddr(id) {
	layer.confirm('是否删除本地址，删除后不可恢复?', {
		btn : [ '确定了!', '考虑下~' ]
	// 按钮
	}, function() {
		$.post(delAddrUrl, {
			"id" : id
		}, function(data) {
			if (data.code == 'success') {
				layer.msg(data.result, {
					icon : 1,
					time : 2000
				// 2秒关闭（如果不配置，默认是3秒）
				});
			} else {
				layer.msg(data.result, {
					icon : 2,
					time : 2000
				// 2秒关闭（如果不配置，默认是3秒）
				});
			}
			window.location.href = window.location.href;
		});
	}, function() {
	});
}

function setAddressDefault(id) {
	layer.confirm('是否将本地址设为默认地址?', {
		btn : [ '确定了!', '考虑下~' ]
	// 按钮
	}, function() {
		$.post(setAddressDefaultUrl, {
			"id" : id
		}, function(data) {
			if (data.code == 'success') {
				layer.msg(data.result, {
					icon : 1,
					time : 2000
				// 2秒关闭（如果不配置，默认是3秒）
				});
			} else {
				layer.msg(data.result, {
					icon : 2,
					time : 2000
				// 2秒关闭（如果不配置，默认是3秒）
				});
			}
			window.location.href = window.location.href;
		});
	}, function() {
	});
}