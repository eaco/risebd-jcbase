$(function () {
	window.e = {};
	e.adaptation = {
		imgSize: function () {
			function size(el, pro) { //首页轮播图高度百分比适配
				var img = el.find('img'),
					afterHeight = img.width() * pro + 'px';
				img.height(afterHeight);
			}

			function listenSize() {
				var elment = $('.bannerList');
				size(elment, 0.428);
			}
			listenSize();
			window.addEventListener('resize', listenSize);
		},
		elSize: function () {
			function size(el, pro) {
				var itemHeight = el.width() * pro + 'px';
				el.height(itemHeight);
			}

			function listenSize() {

				var elment = $('.itemType');
				size(elment, 0.281);
			}

			listenSize();
			window.addEventListener('resize', listenSize);
		}

	}
	e.change = {
		countPrice: function () {
			$('.countTypeNav li').bind('click', function () {
				var thisIndex = $(this).index();
				$(this).addClass('current').siblings().removeClass('current');
				$('.clothesWrap .countBody').eq(thisIndex).show().siblings('.countBody').hide();

			}).eq(0).trigger('click');


		},
		nav: function () {
			$('.changeNav li').bind('click', function () {
				var thisIndex = $(this).index();
				$(this).addClass('current').siblings().removeClass('current');
				$('.changeCt').eq(thisIndex).show().siblings().hide();

			}).eq(0).trigger('click');

		}

	}

	e.adaptation.imgSize();
	e.adaptation.elSize();
	e.change.countPrice();
	e.change.nav();

})