package com.jcbase.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.jcbase.core.BaseConstance;
import com.jcbase.core.auth.interceptor.AuthorityInterceptor;
import com.jcbase.core.auth.interceptor.SysLogInterceptor;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.weixin.interceptor.Oauth2Interceptor;
import com.jcbase.model.RisebdMemberAddress;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;

/**
 * @category 个人中心
 * 
 * @author yiz
 * @date 2016年8月10日 下午3:34:07
 * @version 1.0.0 
 * @copyright 
 */
@Clear({ AuthorityInterceptor.class, SysLogInterceptor.class })
@Before(Oauth2Interceptor.class)
public class UserCenterController extends Controller {

	private static final Logger logger = LoggerFactory.getLogger(UserCenterController.class);

	public void addressList() {
		//得到memberid
		String memberid = getCookie(BaseConstance.MEMBERID_COOKIE_KEY);
		List<RisebdMemberAddress> memberAddresses = RisebdMemberAddress.me.getRisebdMemberAddressByMember(memberid);
		setAttr("memberAddresses", memberAddresses);
		render("addressList.jsp");
	}

	public void editaddress() {
		String id = getPara("id");
		String moduleType = getPara("moduleType");
		RisebdMemberAddress memberAddress = null;
		if (CommonUtils.isNotEmpty(id)) {
			memberAddress = RisebdMemberAddress.me.findById(id);
		}
		//得到当前切换的城市
		String currentCity = getCookie(BaseConstance.CITY_COOKIE_KEY);
		if (CommonUtils.isNotEmpty(currentCity)) {
			try {
				currentCity = URLDecoder.decode(currentCity, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error("异常e={}", e);
			}
		}
		setAttr("memberAddress", memberAddress);
		setAttr("moduleType", moduleType);
		setAttr("currentCity", currentCity);
		render("editaddress.jsp");
	}

	public void delAddr() {
		String id = getPara("id");
		Map<String, Object> mapData = Maps.newHashMap();
		boolean flag = RisebdMemberAddress.me.deleteById(id);
		if (flag) {
			mapData.put("code", "success");
			mapData.put("result", "删除成功");
		} else {
			mapData.put("code", "fail");
			mapData.put("result", "删除失败");
		}
		renderJson(mapData);
	}

	public void setAddressDefault() {
		String id = getPara("id");
		String memberid = getCookie(BaseConstance.MEMBERID_COOKIE_KEY);
		Map<String, Object> mapData = Maps.newHashMap();
		boolean flag = RisebdMemberAddress.me.updateIsDefault(id, memberid);
		if (flag) {
			mapData.put("code", "success");
			mapData.put("result", "设置成功");
		} else {
			mapData.put("code", "fail");
			mapData.put("result", "设置失败");
		}
		renderJson(mapData);
	}

	/**
	 * @category 新增/修改用户地址
	 */
	public void doEditAddress() {
		Map<String, Object> mapData = Maps.newHashMap();
		String memberid = getCookie(BaseConstance.MEMBERID_COOKIE_KEY);
		if (CommonUtils.isEmpty(memberid)) {
			mapData.put("code", "fail");
			mapData.put("result", "操作失败");
			renderJson(mapData);
			return;
		}
		String id = getPara("id");
		String mobile = getPara("mobile");
		String username = getPara("username");
		String longitude = getPara("longitude");
		String latitude = getPara("latitude");
		String province = getPara("province");
		String city = getPara("city");
		String area = getPara("area");
		String k_province = getPara("k_province");
		String k_city = getPara("k_city");
		String k_area = getPara("k_area");
		String address = getPara("address");
		String is_defaultStr = getPara("is_default");
		if (CommonUtils.isEmpty(is_defaultStr)) {
			is_defaultStr = "0";
		}
		Integer is_default = Integer.parseInt(is_defaultStr);
		int result = 0;
		try {
			result = RisebdMemberAddress.me.saveAddress(id, memberid, mobile, username, longitude, latitude, province,
					city, area, k_province, k_city, k_area, address, is_default);
		} catch (Exception e) {
			logger.error("新增用户地址出现异常e={}", e);
		}
		if (result == 1) {
			mapData.put("code", "success");
			mapData.put("result", "新增成功");
		} else if (result == 2) {
			mapData.put("code", "success");
			mapData.put("result", "修改成功");
		} else {
			mapData.put("code", "fail");
			mapData.put("result", "操作失败");
		}
		renderJson(mapData);
	}

	public void me() {
		render("me.jsp");
	}
}
