package com.jcbase.controller.sys;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdCouponMember;
import com.jcbase.model.RisebdCouponRule;
import com.jcbase.model.RisebdMember;
import com.jfinal.plugin.activerecord.Page;

/**
 * @category 优惠券领取情况
 * 
 * @author yiz
 * @date 2016年8月10日 下午7:17:09
 * @version 1.0.0 
 * @copyright 
 */
public class CouponMemberController extends JCBaseController {
	@RequiresPermissions(value = { "/sys/couponMember" })
	public void index() {
		this.renderJsp("coupon_member_index.jsp");
	}

	public void getListData() {
		String couponNo = this.getPara("couponNo");
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(couponNo)) {
			conditions.add(new Condition("coupon_no", Operators.EQ, couponNo));
		}
		conditions.add(new Condition("status", Operators.NOT_EQ, 5));
		Page<RisebdCouponMember> pageInfo = RisebdCouponMember.me.getPage(getPage(), this.getRows(), conditions,
				this.getOrderby());
		List<RisebdCouponMember> risebdCouponMembers = pageInfo.getList();
		for (RisebdCouponMember couponMember : risebdCouponMembers) {
			RisebdCouponRule risebdCouponRule = RisebdCouponRule.me.getById(couponMember.getRuleId());
			if (null != risebdCouponRule) {
				couponMember.put("risebdCouponRule", risebdCouponRule);
			}
			if (CommonUtils.isNotEmpty(couponMember.getMemberId())) {
				RisebdMember risebdMember = RisebdMember.me.getById(couponMember.getMemberId());
				couponMember.put("risebdMember", risebdMember);
			}
		}
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}
}
