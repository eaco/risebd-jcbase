package com.jcbase.controller.sys;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdMember;
import com.jfinal.plugin.activerecord.Page;

/**
 * @category 会员管理
 * 
 * @author yiz
 * @date 2016年7月28日 上午9:41:46
 * @version 1.0.0 
 * @copyright eisebd.cn 
 */
public class MemberController extends JCBaseController {
	private static Logger logger = LoggerFactory.getLogger(MemberController.class);

	@RequiresPermissions(value = { "/sys/member" })
	public void index() {
		render("member_index.jsp");
	}

	public void getListData() {
		String name = this.getPara("name");
		String mobile = this.getPara("mobile");
		Integer subscribe = this.getParaToInt("subscribe");
		//来源，关注或者授权
		logger.info("请求参数name=" + name + ",mobile=" + mobile + ",subscribe=" + subscribe);
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(name)) {
			conditions.add(new Condition("name", Operators.LIKE, name));
		}
		if (CommonUtils.isNotEmpty(mobile)) {
			conditions.add(new Condition("mobile", Operators.LIKE, mobile));
		}
		if (null != subscribe) {
			conditions.add(new Condition("subscribe", Operators.EQ, subscribe));
		}
		Page<RisebdMember> pageInfo = RisebdMember.me.getPage(getPage(), this.getRows(), conditions, this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}
}
