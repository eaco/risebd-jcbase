$(function () {
    e.swiper = {
        banner: function () {
            var swiper = new Swiper('.banner-swiper-container', {
                pagination: '.swiper-pagination',
                slidesPerView: 1,
                loop: true,
                autoplay: 3000,
                lazyLoading: true
            });
        },
        guide: function () {
            var swiper = new Swiper('.guide-swiper-container', {
                slidesPerView: 1,
                lazyLoading: true
            });
        }
    }
    e.swiper.banner();
    e.swiper.guide();
})