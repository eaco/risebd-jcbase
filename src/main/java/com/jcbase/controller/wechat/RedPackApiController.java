package com.jcbase.controller.wechat;

import javax.servlet.http.HttpServletRequest;

import com.jcbase.core.auth.interceptor.AuthorityInterceptor;
import com.jcbase.core.auth.interceptor.SysLogInterceptor;
import com.jcbase.core.util.ReadPackUtils;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.kit.PropKit;

/**
 * @category 微信红包
 * @author yiz
 * 2016年5月28日
 */
@Clear({ AuthorityInterceptor.class, SysLogInterceptor.class })
public class RedPackApiController extends Controller {
	private String sendName = PropKit.get("sendName");
	//微信证书路径
	private String certPath = PropKit.get("certPath");
	//商户相关资料
	String wxappid = PropKit.get("appId");
	// 微信支付分配的商户号
	String partner = PropKit.get("mch_id");
	//API密钥
	String paternerKey = PropKit.get("paternerKey");

	/**
	 * @category 普通红包
	 */
	public void send() {
		HttpServletRequest request = getRequest();
		String total_amount = getPara("total_amount");
		String total_num = getPara("total_num");
		String wishing = getPara("wishing");
		String act_name = getPara("act_name");
		String remark = getPara("remark");
		String reOpenid = getPara("reOpenid");
		//发送红包
		boolean isSend = ReadPackUtils.send(request, total_amount, total_num, wishing, act_name, remark, reOpenid,
				partner, wxappid, sendName, paternerKey, certPath);
		renderJson(isSend);
	}

	/**
	 * @category 发送裂变红包
	 */
	public void sendGroupRedPack() {
		HttpServletRequest request = getRequest();
		String total_amount = getPara("total_amount");
		String total_num = getPara("total_num");
		String wishing = getPara("wishing");
		String act_name = getPara("act_name");
		String remark = getPara("remark");
		String reOpenid = getPara("reOpenid");
		//发送裂变红包
		boolean isSend = ReadPackUtils.sendGroupRedPack(request, partner, wxappid, sendName, reOpenid, total_amount,
				total_num, "ALL_RAND", wishing, act_name, remark, paternerKey, certPath);
		renderJson(isSend);
	}

	/**
	 * @category 查询红包接口
	 */
	public void query() {
		String mch_billno = getPara("mch_billno");
		String query = ReadPackUtils.query(mch_billno, partner, wxappid, paternerKey, certPath);
		renderJson(query);
	}

}