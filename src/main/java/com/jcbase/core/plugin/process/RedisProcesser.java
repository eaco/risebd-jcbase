package com.jcbase.core.plugin.process;

/**
 * 
 * @category Redis 消息处理器
 * 
 * @author yi.z
 * @date 2016-1-12 下午2:23:04
 * @version 1.0.0 
 * @copyright risebd.cn
 */
public interface RedisProcesser {
	/**
	 * @category 执行处理器
	 * @description doProcess
	 * @param queueName
	 * @param content
	 */
	void doProcess(String queueName, String content);
}
