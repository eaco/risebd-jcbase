package com.jcbase.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.jcbase.core.BaseConstance;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.MathUtil;
import com.jcbase.dto.CarDTO;
import com.jcbase.model.RisebdCoupon;
import com.jcbase.model.RisebdMember;
import com.jfinal.plugin.redis.Redis;

/**
 * @category 缓存服务，获取当前的缓存，用于各个设置，穿透数据库的操作
 * @author yiz
 *
 */
public class CacheService {

	/**
	 * @category 会员缓存集合添加会员id
	 * @param memberId
	 */
	public static void addMemberIdSetCache(String memberId) {
		Redis.use().sadd(BaseConstance.MEMBER_ID_SET_CACHE, memberId);
	}

	/**
	 * @category 得到会员Id集合
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Set<String> getMemberIds() {
		Set<String> memberIds = Redis.use().smembers(BaseConstance.MEMBER_ID_SET_CACHE);
		return memberIds;
	}

	/**
	 * @category 得到某优惠券规则下已发送的会员
	 * @param couponRuleId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Set<String> getCouponRuleMemberIds(String couponRuleId) {
		Set<String> memberIds = Redis.use()
				.smembers(BaseConstance.MEMBER_ID_SET_COUPONRULE_CACHE_PREFIX + couponRuleId);
		return memberIds;
	}

	/**
	 * @category 两个集合之间差集
	 * @param couponRuleId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Set<String> getDiffMemberIds(String... keys) {
		Set<String> memberIds = Redis.use().sdiff(keys);
		return memberIds;
	}

	/**
	 * @category 根据会员ID获取openid，优先级：redis->cache->db
	 * @param memberId
	 * @return
	 */
	public static String getOpenidByMemberId(String memberId) {
		String openid = Redis.use().get(BaseConstance.MEMBER_ID_OPENID_SINGLE_CAHCE_PREFIX + memberId);
		if (CommonUtils.isEmpty(openid)) {
			openid = RisebdMember.me.getOpenidById(memberId);
			Redis.use().set(BaseConstance.MEMBER_ID_OPENID_SINGLE_CAHCE_PREFIX + memberId, openid);
		}
		return openid;
	}

	/**
	 * @category 加入优惠券的缓存
	 * @param ruleId
	 * @param noSendMemberIdArray
	 */
	public static void addCouponRuleMemberIds(String ruleId, String[] noSendMemberIdArray) {
		Redis.use().sadd(BaseConstance.MEMBER_ID_SET_COUPONRULE_CACHE_PREFIX + ruleId, noSendMemberIdArray);
	}

	/**
	 * @category 移除优惠券的缓存
	 * @param memberId
	 */
	public static void removeMemberIdSetCache(String memberId) {
		Redis.use().srem(BaseConstance.MEMBER_ID_SET_CACHE, memberId);
	}

	/**
	 * @category 添加优惠券规则
	 * @param rule_id
	 * @param couponNos
	 * @return
	 */
	public static long addCouponNoRuleUsable(String rule_id, List<String> couponNos) {
		if (null != couponNos && couponNos.size() > 0) {
			return Redis.use().lpush(BaseConstance.COUPON_NO_RULE_USABLE_PREFIX + rule_id,
					couponNos.toArray(new String[] {}));
		}
		return 0;
	}

	/**
	 * @category 返回优惠券可以进行发送使用的集合
	 * @param ruleId
	 * @param num 需要取出的index编号
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<String> getCouponNoRuleUsable(String rule_id, Integer status, Long coupon_num, int num) {
		if (status != 1) {//当不可领取时直接返回
			return null;
		}
		List<String> usableNos = Redis.use().lrange(BaseConstance.COUPON_NO_RULE_USABLE_PREFIX + rule_id, 0, num);
		if (CommonUtils.isEmpty(usableNos)) {//
			//查询数据库，返回可以使用的优惠券编号集合
			usableNos = RisebdCoupon.me.getAllUsableCouponNosByRuleId(rule_id, coupon_num, num);
		}
		return usableNos;
	}

	/**
	 * @category 删除
	 * @param rule_id
	 * @param status
	 * @param recivedCouponNos
	 * @return
	 */
	public static long removeCouponNoRuleUsable(String rule_id, Integer status, List<String> recivedCouponNos) {
		if (status != 1) {//当不可领取时直接返回
			return 0;
		}
		if (null == recivedCouponNos || recivedCouponNos.size() == 0) {
			return 0;
		}
		Long usableNos = Redis.use().lrem(BaseConstance.COUPON_NO_RULE_USABLE_PREFIX + rule_id, recivedCouponNos.size(),
				recivedCouponNos.toArray(new String[] {}));
		return usableNos;
	}

	/**
	 * @category 得到购物车数量
	 * @param memberId
	 * @return
	 */
	public static int getShopCarNum(String memberId) {
		String carResult = Redis.use().get(BaseConstance.MEMBER_SHOP_CAR_CACHE_PREFIX + memberId);
		JSONArray jsonArray = null;
		int carSize = 0;
		if (CommonUtils.isNotEmpty(carResult)) {
			//转换成对象
			jsonArray = JSON.parseArray(carResult);
		}
		if (null != jsonArray) {
			carSize = jsonArray.size();
		}
		return carSize;
	}

	/**
	 *  得到购物车信息
	 * @param memberId
	 * @return
	 */
	public static Map<String, Object> getShopCar(String memberId) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String carResult = Redis.use().get(BaseConstance.MEMBER_SHOP_CAR_CACHE_PREFIX + memberId);
		JSONArray jsonArray = null;
		if (CommonUtils.isNotEmpty(carResult)) {
			//转换成对象
			jsonArray = JSON.parseArray(carResult);
		} else {
			jsonArray = new JSONArray();
		}
		double totlePrice = 0;
		List<CarDTO> carDTOs = new ArrayList<>();
		for (Object object : jsonArray) {
			CarDTO cacheShoppingDTO = JSON.parseObject(JSON.toJSONString(object), CarDTO.class);
			carDTOs.add(cacheShoppingDTO);
			double num = cacheShoppingDTO.getNum();
			double nowPrice = cacheShoppingDTO.getNowPrice();
			double currTotlePrice = MathUtil.mul(num, nowPrice);
			totlePrice = MathUtil.add(totlePrice, currTotlePrice);
		}
		resultMap.put("totlePrice", totlePrice);
		resultMap.put("carDTOs", carDTOs);
		return resultMap;
	}
}
