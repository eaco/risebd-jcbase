<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<link rel="stylesheet" href="https://res.wx.qq.com/open/libs/weui/0.4.3/weui.min.css"/>
<link rel="stylesheet" href="${res_url}css/appointment/common.css"/>
<link rel="stylesheet" href="${res_url}css/appointment/main.css"/>
<link rel="stylesheet" href="${res_url}css/appointment/main.ext.css"/>
<link rel="stylesheet" href="${res_url}bower_components/Swiper/dist/css/swiper.min.css">