<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<jsp:include page="../css.jsp" flush="true" />
</head>

<body>
	<div class="main addressWrap">
		<a href="/uc/editaddress?moduleType=1" class="addAddress">
	          	  添加收货地址  <i class="addIcon">+</i>
        </a>
		<ul class="addressBody">
			<c:forEach items="${memberAddresses }" var="memberAddress">
			<c:choose>
				<c:when test="${memberAddress.isDefault>0 }">
					<li class="itmes default">
				</c:when>
				<c:otherwise>
					<li class="itmes ">
					<span class="setdefault" onclick="setAddressDefault('${memberAddress.id}')">设为默认</span>
				</c:otherwise>
			</c:choose>
				<ul class="addressInfo ">
					<li class="userInfo">
						<span class="userName">${memberAddress.username }</span>
						<span class="phone">${memberAddress.mobile }</span>
					</li>
					<li>
						${memberAddress.province }${memberAddress.city }${memberAddress.area }${memberAddress.address }
					</li>

				</ul>
				<div class="addressFooter">
					<ul class="addressDo ">

						<li><a href="/uc/editaddress?id=${memberAddress.id }&moduleType=2" class="edit">修改</a></li>
						<li class="js-del" onclick="delAddr('${memberAddress.id}')">删除</li>

					</ul>

				</div>
			
			</li>
			</c:forEach>
		</ul>
		
</div>
	<jsp:include page="../js.jsp" flush="true" />
	<script type="text/javascript" src="${res_url}js/appointment/address.js"></script>
</body>

</html>