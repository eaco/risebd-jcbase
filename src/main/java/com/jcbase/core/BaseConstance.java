package com.jcbase.core;

import com.jfinal.kit.PropKit;

public class BaseConstance {

	/**
	 * @category 优惠券desc加密key
	 */
	public static final String DES_COUPON_KEY = "coupons for system";

	/**
	 * @category 系统领域配置
	 */
	public static final String DOMAIN = PropKit.get("domain");

	/**
	 * @category 单个memberId缓存openid
	 */
	public static final String MEMBER_ID_OPENID_SINGLE_CAHCE_PREFIX = "MEMBER_ID_OPENID_SINGLE_CAHCE_";

	/**
	 * @category 会员id的缓存集合
	 */
	public static final String MEMBER_ID_SET_CACHE = "MEMBER_ID_SET_CACHE";

	/**
	 * @category 缓存已发送某个优惠券规则的会员集合前缀，后缀为优惠券规则的id
	 */
	public static final String MEMBER_ID_SET_COUPONRULE_CACHE_PREFIX = "MEMBER_ID_SET_COUPONRULE_CACHE_";
	/**
	 *  @category 购物车
	 */
	public static final String MEMBER_SHOP_CAR_CACHE_PREFIX = "MEMBER_SHOP_CAR_CACHE_";

	/**
	 * @category 是否需要关注
	 */
	public static final boolean NEED_FOLLOW = PropKit.getBoolean("needFollow", true);

	/**
	 * @category openid cookie的缓存
	 */
	public static final String OPENID_COOKIE_KEY = PropKit.get("openid_cookie_key");
	/**
	 * @category member cookie的缓存
	 */
	public static final String MEMBERID_COOKIE_KEY = PropKit.get("memberid_cookie_key");
	/**
	 * @category 当前城市 cookie的缓存
	 */
	public static final String CITY_COOKIE_KEY = PropKit.get("city_cookie_key");

	/**
	 * @category openid保存cookie的时间
	 */
	public static final int OPENID_COOKIE_MAXAGE = 20 * 24 * 3600;

	/**
	 * @category 静态链接地址
	 */
	public static final String STATIC_URL = PropKit.get("static_url");

	public static final String SYS_MGR = "/sys";

	/**
	 * @category 微信首页
	 */
	public static final String WE_INDEX = PropKit.get("we_index");

	/**
	 * @category 微信个人中心
	 */
	public static final String WE_ME = PropKit.get("we_me");

	/**
	 * @category 微信回复消息
	 */
	public static final String WX_RESPONSE = "WX_RESPONSE";

	/**
	 * @category 优惠券规则下待领取的优惠券集合，此处才有redis List
	 */
	public static final String COUPON_NO_RULE_USABLE_PREFIX = "COUPON_NO_RULE_USABLE_";

	/**
	 * @category 优惠券规则下已领取的优惠券集合，此处才有redis List
	 */
	public static final String COUPON_NO_RULE_UNUSABLE_PREFIX = "COUPON_NO_RULE_UNUSABLE_";

	/**
	 * 订单前缀
	 */
	public static final String APPOINT_ORDER_PREFIX = "DD";
}
