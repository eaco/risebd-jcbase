<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<jsp:include page="/WEB-INF/view/common/basecss.jsp" flush="true" />
</head>

<body class="no-skin">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<div class="main-content" style="margin-left: 0px;">
					<div class="page-content">
						<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<form class="form-horizontal" id="validation-form" method="post">
													<input name="id" type="hidden" value="${couponRule.id}"/>
													<input name="status" type="hidden" value="${couponRule.status }">
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="title">标题</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="title" id="title" value="${couponRule.title}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="type">类型</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="type" id="type" class="col-xs-12 col-sm-6">
																	<option value="1">新用户注册奖励</option>
																	<option value="2">推广奖励</option>
																	<option value="3">被推广奖励</option>
																	<option value="4">活动自动赠送</option>
																	<option value="5">活动领取</option>
																	<option value="6">用于用户兑换</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="conpon_form">折扣形式</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="conpon_form" id="conpon_form" class="col-xs-12 col-sm-6">
																	<option value="1">抵扣券</option>
																	<option value="2">折扣券</option>
																</select>
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="coupon_amount">优惠力度(与折扣形式相关,元/折)</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="coupon_amount" id="coupon_amount" value="${couponRule.conponForm}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="coupon_num">优惠券数量(0表示无限)</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="coupon_num" id="coupon_num" value="${couponRule.couponNum}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="get_times">每人限量(0表示不限)</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="get_times" id="get_times" value="${couponRule.getTimes}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="get_times">优惠使用最小消费(0表示不限)</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="limit_amount" id="limit_amount" value="${couponRule.couponAmount}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													<!-- <div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">状态</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="status" id="status" class="col-xs-12 col-sm-6">
																	<option value="0">已生成规则</option>
																	<option value="1">已完成优惠券的生成</option>
																	<option value="2">可领取的优惠券</option>
																	<option value="3">超时不可领取</option>
																</select>
															</div>
														</div>
													</div> -->
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="start_timing">定时发送优惠券(限活动自动赠送)</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" onfocus="WdatePicker({dateFmt: 'yyyy-MM-dd HH:mm:ss'})" name="start_timing" id="start_timing" value='<fmt:formatDate value="${couponRule.startTiming}" pattern="yyyy-MM-dd HH:mm:ss"/>' class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="coupon_start_time">有效开始时间</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'coupon_end_time\')||\'%y-%M-%d\'}'})" name="coupon_start_time" id="coupon_start_time" value='<fmt:formatDate value="${couponRule.couponStartTime}" pattern="yyyy-MM-dd"/>'  class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="coupon_end_time">有效结束时间</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'coupon_start_time\')||\'%y-%M-%d\'}'})" name="coupon_end_time" id="coupon_end_time" value='<fmt:formatDate value="${couponRule.couponEndTime}" pattern="yyyy-MM-dd"/>' class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="mark">内容</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <textarea rows="3" cols="34" name="mark" id="mark">${couponRule.mark }</textarea>
															</div>
														</div>
													</div>
													<div class="clearfix form-actions" align="center">
														<div class="col-md-offset-3 col-md-9">
															<button id="submit-btn" class="btn btn-info" type="submit" data-last="Finish">
																<i class="ace-icon fa fa-check bigger-110"></i>
																提交
															</button>
															&nbsp; &nbsp; &nbsp;
															<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												重置
											</button>
														</div>
													</div>
												</form>
						</div><!-- /.col -->
					</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
		</div><!-- /.main-container -->
			<!-- basic scripts -->
<jsp:include page="/WEB-INF/view/common/basejs.jsp" flush="true" />
<script src="/res/js/ztree/js/jquery.ztree.core-3.5.min.js"></script>
	<script type="text/javascript">
			function defaultSelect(id,value){
				if(value){
					//默认选中
					$("#"+id).val(value);
				}
			}
			
			function compareDate(beginDate,endDate){
				 var d1 = new Date(beginDate.replace(/\-/g, "\/")); 
				 var d2 = new Date(endDate.replace(/\-/g, "\/"));  
			    if(d1 > d2){
			        return true;
			    } else {
			       return false;
			    }
			}
			$(document).ready(function(){
				defaultSelect("status","${couponRule.status}");
				defaultSelect("conpon_form","${couponRule.conponForm}");
				defaultSelect("type","${couponRule.type}");
				initformSubmitEvent();
			});
			
			var setting = {
			view: {
				dblClickExpand: false
			},
			data: {
				simpleData: {
				enable: true
			}
			},
			callback: {
				beforeClick: beforeClick,
				onClick: onClick
			}
			};
			
			var $validation = true;
			function initformSubmitEvent(){
				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					rules: {
						title:{
							required: true
						},
						type:{
							required: true
						},
						status:{
							required: true
						},
						conpon_form:{
							required:true
						},
						coupon_start_time:{
							required:true
						},
						coupon_end_time:{
							required:true
						},
						coupon_num:{
							digits:true
						},
						get_times:{
							digits:true,
							max:9999,
						},
						limit_amount:{
							number:true,
							max:1000,
						},
						coupon_amount:{
							required:true,
							number:true,
							max:1000,
							min:0.0000001
						},
						
					},
					messages: {
						title:{
							required: "请输入标题"
						},
						type:{
							required: "请选择类型"
						},
						status:{
							required:"请输入选择状态"
						},
						conpon_form:{
							required: "请选择折扣形式"
						},
						coupon_start_time:{
							required: "请输入优惠券开始时间"
						},
						coupon_end_time:{
							required: "请输入优惠券结束时间"
						},
						coupon_num:{
							digits:"优惠券数量只能输入整数",
							max:"最大不超过9999",
						},
						get_times:{
							digits:"每人限量只能为整数"
						},
						limit_amount:{
							number:"最小消费金额必须为数字",
							max:"最大不超过1000",
						},
						coupon_amount:{
							required: "请输入优惠力度",
							number:"优惠力度金额必须为数字",
							max:"最大不超过1000",
							min:"必须大于0"
						}
					},
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
						var $form = $("#validation-form");
						var $btn = $("#submit-btn");
						if($btn.hasClass("disabled")) return;
						$btn.addClass("disabled");
						 var postData=$("#validation-form").serialize();
						 
						 var typeValue = $("#type").val();
						 var conponFormValue = $("#conpon_form").val();
						 var startTimingValue = $("#start_timing").val();
						 var couponEndTimeValue = $("#coupon_end_time").val();
						 var couponStartTimeValue = $("#coupon_start_time").val();
						 if(typeValue!='4'){
							 //当非活动自动赠送时，不需要设置自动发送时间
							 if(startTimingValue){
								 layer.msg('非活动自动赠送，不需要设置自动发送时间!', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			}); 
								 $btn.removeClass("disabled");
								 return;
							 }
						 }else{
							 var compareEndValue = compareDate(startTimingValue,couponEndTimeValue);
							 
							 var compareStartValue = compareDate(startTimingValue,couponStartTimeValue);
							 if(compareEndValue || !compareStartValue){
								 layer.msg('设置自动发送时间不可大于结束时间或小于开始时间!', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			}); 
								 $btn.removeClass("disabled");
								 return;
							 }
						 }
			        	 $.post("/sys/couponRule/saveRes" ,postData,function(data){
			        		if(data.code=='success'){
			        			layer.msg('操作成功', {
			        				icon: 1,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			},function(){
			        				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			        				parent.reloadGrid();
			        				parent.layer.close(index); //再执行关闭 
			        			});
			        		}else{
			        			layer.msg(data.result, {
			        				icon: 2,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			});
			        		}
			        		$btn.removeClass("disabled");
			        	},"json");
						return false;
					},
					invalidHandler: function (form) {
					}
				});
			}
		</script>
</body>

</html>

