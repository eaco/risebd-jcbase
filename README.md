#risebd-jcbase

 **请不要乱修改我提交配置或者连接服务器，否则项目将给予关闭** 


本项目大量使用到redis，微信消息异步处理，优惠券的发放，使用到redis的List和Set等功能。
古需要下载redis并配置，调试时可自行下载window版本的redis
下载地址[redis-windows](https://github.com/ServiceStack/redis-windows)

项目使用到任务拆分技术，即Fork/Join框架，提高项目处理逻辑的速度，尤其是部署在多核服务器上时，速度将能够达到平时服务器的几倍，甚至是十几倍。
此框架在jdk1.7开始才开始支持，故需要使用JDK7以上版本。
此方面的知识可参考alibaba大神 清英 的博客：[Fork/Join框架介绍](http://ifeve.com/talk-concurrency-forkjoin/),本人知识水平暂时有限，有问题指点我一二。

项目演示地址:http://edx.risebd.cn/sys/   admin 123456  (大佬们别删除我的数据，数据库可没有备份)

前台功能也在同步进行，目前需要一套基于微信样式的O2Ohtml，有的话可与我联系 email: zhao_yi_yun@163.com 微信:Eaco-eaco

项目演示截图：
![优惠券](http://git.oschina.net/uploads/images/2016/0822/150132_0eaca23c_532407.png "优惠券规则")
![菜单](http://git.oschina.net/uploads/images/2016/0822/150230_bce916d9_532407.png "微信菜单")
![首页](http://git.oschina.net/uploads/images/2016/0828/175603_c3dc6468_532407.png "前台首页")
1. 微信公众号设置
    - 微信菜单设置（已完成）
    - 微信自动回复设置(已完成，图片人脸识别)
        使用redis对微信消息异步队列处理，不使用jfinal-wechat的消息回复，目前后台可配置，文本、图片、单图文的回复。
    - 微信支付设置(已完成，暂未测试)
2. 会员管理（已完成后台管理，会员优惠券，会员地址暂未完成）
3. 产品管理(已完成部分)
    门店产品分配暂未完成，缺少交互，之后完成此模块
4. 门店管理(已完成)
5. 订单管理
6. 优惠券管理(已完成优惠券规则部分，优惠券列表与强制收回等功能暂未完成,已使用fork/join+定时任务完成优惠券自动发送功能，同时调用微信模板发送接口进行发送模板消息)
7. 日志管理（已完成）
8. 权限管理（已完成）

