package com.jcbase.core.weixin.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcbase.core.util.WeiXinUtils;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.weixin.sdk.api.ApiConfigKit;

/**
 * @category 微信拦截器
 * @author yiz
 *
 */
public class ApiInterceptor implements Interceptor {
	private static final Logger logger = LoggerFactory.getLogger(ApiInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		logger.info("Api拦截器开始");
		String toUserName = inv.getController().getPara("toUserName");
		try {
			ApiConfigKit.setThreadLocalApiConfig(WeiXinUtils.getApiConfig(toUserName));
			inv.invoke();
		} finally {
			ApiConfigKit.removeThreadLocalApiConfig();
		}
	}

}
