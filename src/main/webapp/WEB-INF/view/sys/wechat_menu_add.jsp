<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<jsp:include page="/WEB-INF/view/common/basecss.jsp" flush="true" />
<link rel="stylesheet" href="${res_url}js/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
</head>

<body class="no-skin">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<div class="main-content" style="margin-left: 0px;">
					<div class="page-content">
						<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<form class="form-horizontal" id="validation-form" method="post">
													<div class="form-group">
														<input name="id" type="hidden" value="${wechatMenu.id}"/>
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="parent_name">上级菜单</label>
														<div class="col-xs-12 col-sm-9">
															<div class="input-icon input-icon-right" id="kka">
																<input type="hidden" id="parent_id" name="parent_id" value="${wechatMenu.parentId}">
													            <input type="text" readonly="readonly" id="parent_name" name="parent_name" value="${wechatMenu.parentName}" class="col-xs-12 col-sm-6">
													            <button id="menuBtn" type="button" class="btn btn-sm btn-default" onclick="showMenu(); return false;" style="float: left;">选择</button>
															</div>
															<div id="menuContent" class="menuContent" style="overflow-y:auto; overflow-x:auto;display:none;position:absolute; z-index: 99999;background-color: #FFFFFF;border: 1px solid #858585;height: 250px;width: 270px;">
																 <ul id="treeDemo" class="ztree" ></ul>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">菜单名称</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="name" id="name" value="${wechatMenu.name}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">菜单类型</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <select name="type" id="type"  class="col-xs-12 col-sm-6"  >
																	<option value="view">跳转链接</option>
																	<option value="click">点击事件</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="url">菜单地址</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="url" id="url" value="${wechatMenu.url}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="key">菜单事件Key</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="key" id="key" value="${wechatMenu.key}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">排序号</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
													            <input type="text" name="orders" id="orders" value="${wechatMenu.orders}" class="col-xs-12 col-sm-6">
															</div>
														</div>
													</div>
													
													<div class="clearfix form-actions" align="center">
														<div class="col-md-offset-3 col-md-9">
															<button id="submit-btn" class="btn btn-info" type="submit" data-last="Finish">
																<i class="ace-icon fa fa-check bigger-110"></i>
																提交
															</button>
															&nbsp; &nbsp; &nbsp;
															<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												重置
											</button>
														</div>
													</div>
												</form>
						</div><!-- /.col -->
					</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
		</div><!-- /.main-container -->
			<!-- basic scripts -->
<jsp:include page="/WEB-INF/view/common/basejs.jsp" flush="true" />
<script src="/res/js/ztree/js/jquery.ztree.core-3.5.min.js"></script>
	<script type="text/javascript">
			function defaultSelect(id,value){
				//默认选中
				if(value){
					$("#"+id).val(value);
				}
			}
			$(document).ready(function(){
				initformSubmitEvent();
				$.fn.zTree.init($("#treeDemo"), setting, zNodes);
				defaultSelect("type","${wechatMenu.type}");
			});
			
			var setting = {
			view: {
				dblClickExpand: false
			},
			data: {
				simpleData: {
				enable: true
			}
			},
			callback: {
				beforeClick: beforeClick,
				onClick: onClick
			}
			};
			
			//json数据源，也可以从后台读取json字符串，并转换成json对象，如下所示
			var strNodes = '${jsonTree}';
			var zNodes = eval("("+strNodes+")"); //将json字符串转换成json对象数组，strNode一定要加"（）"，不然转不成功
			function beforeClick(treeId, treeNode) {
				var check = (treeNode.id!=10000);
			if (!check) alert("这是标题，不能选！");
				return check;
			}
			function onClick(e, treeId, treeNode) {
				$("#parent_id").val(treeNode.id);
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getSelectedNodes(),
				v = "";
				nodes.sort(function compare(a,b){return a.id-b.id;});
			for (var i=0, l=nodes.length; i<l; i++) {
				v += nodes[i].name + ",";
			}
				if (v.length > 0 ) v = v.substring(0, v.length-1);
				var cityObj = $("#parent_name");
				cityObj.attr("value", v); 
			}
			
			function showMenu() {
				var cityObj = $("#parent_name");
				var cityOffset = $("#parent_name").offset();
				var sel=document.getElementById("kka");
				$("#menuContent").css({left:sel.offsetLeft + "px", top:sel.offsetTop + cityObj.outerHeight() + "px"}).slideDown("fast");
				$("body").bind("mousedown", onBodyDown);
			}
			function hideMenu() {
				$("#menuContent").fadeOut("fast");
				$("body").unbind("mousedown", onBodyDown);
			}
			function onBodyDown(event) {
				if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
					hideMenu();
				}
				if(event.target.id.length>=15&&event.target.id.length!=17){
					hideMenu();
				}
			}
			var $validation = true;
			function initformSubmitEvent(){
				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					rules: {
						name:{
							required: true
						},
						url:{
							url:true
						},
						orders:{
							required: true,
							digits:true
						}
					},
					messages: {
						name:{
							required: "请输入名称"
						},
						url:{
							url: "请输入正确链接地址"
						},
						orders:{
							required: "请输入排序号",
							digits:"排序号只能输入整数"
						}
					},
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
						var $form = $("#validation-form");
						var $btn = $("#submit-btn");
						if($btn.hasClass("disabled")) return;
						$btn.addClass("disabled");
						 var postData=$("#validation-form").serialize();
						 
						 var typeVal = $("#type").val();
						 var urlVal = $("#url").val();
						 var keyVal = $("#key").val();
						 if(typeVal=='view'){
							 if(!urlVal){
								 layer.msg('必须输入有效的跳转链接地址', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			});
								 $btn.removeClass("disabled");
									return;
							 }
							 
							 if(keyVal){
								 layer.msg('不可输入事件key', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			});
								 $btn.removeClass("disabled");
									return;
							 }
						 }
						 if(typeVal=='click'){
							 if(!keyVal){
								 layer.msg('必须输入有效key', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			});
								 $btn.removeClass("disabled");
									return;
							 }
							 
							 if(urlVal){
								 layer.msg('不可输入跳转链接地址', {
				        				icon: 2,
				        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
				        			});
								 $btn.removeClass("disabled");
									return;
							 }
						 }
			        	 $.post("/sys/wechat/menu/saveRes" ,postData,function(data){
			        		if(data.code=='success'){
			        			layer.msg('操作成功', {
			        				icon: 1,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			},function(){
			        				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			        				parent.reloadGrid();
			        				parent.layer.close(index); //再执行关闭 
			        			});
			        		}
			        		$("#submit-btn").removeClass("disabled");
			        	},"json");
						return false;
					},
					invalidHandler: function (form) {
					}
				});
			}
		</script>
</body>

</html>

