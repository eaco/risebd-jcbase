<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,user-scalable=0">
<jsp:include page="../css.jsp" flush="true" />
<title>用户中心</title>
</head>


<body>
	<input type="hidden" id="openid" value="${openid }">
	<div class="main pFooter">
		<div class="meWrap">
			<div class="idCardWrap">
				<div class="idCardBody flex flex-aicenter">
					<div class="avatarWrap">
						<img src="${member['headimgurl']}" alt="" class="avatar">
					</div>
					<div class="idBodyRight">
						<span class="userID">账户： <c:choose>
								<c:when test="${empty member['mobile'] }">
                        		${member['mobile']}
                        	</c:when>
								<c:otherwise>
                        		${member['mobile'] }
                        	</c:otherwise>
							</c:choose>
						</span> <a href="/usercenter/recharge"
							class="btn goRechargeBtn">充值</a>
					</div>

				</div>
				<div class="idCardFooter">
					<p>
						余额：<span class="num"><i class="RMB">￥</i>${member['account'] }</span>
					</p>
					<p>
						<a href="/usercenter/couponlist">优惠券:<i
							class="num">${useableCouponNum }</i>张
						</a>
					</p>
				</div>
			</div>
			<div class="settingWrap">
				<ul class="settingList">

					<li><a href="/uc/addressList"><span>常用地址</span>
					</a></li>

					<!--<li><a href="/usercenter/recommend?fromOpenid=${openid }"><span>推荐有奖</span></a></li>-->
					<li><a href="/usercenter/integral"><span>我的积分</span><span
							class="integral">${member.integral }分</span></a></li>
					<li><a href="/usercenter/opinionsurvey"><span>意见反馈</span></a></li>
					<!-- <li><a href=""><span>绑定邮箱</span></a></li> -->
					<li><a href="tel:${telPhone }"><span>客服热线</span><span
							class="serviceTel">${telPhone }</span></a></li>
				</ul>
			</div>

		</div>
	</div>
	<jsp:include page="./bottom.jsp" flush="true" />
	<jsp:include page="../js.jsp" flush="true" />
</body>
</html>