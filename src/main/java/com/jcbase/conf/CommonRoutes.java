package com.jcbase.conf;

import com.jcbase.controller.ExtController;
import com.jcbase.controller.UploadController;
import com.jfinal.config.Routes;

/**
 * @category 通用配置
 * @author yiz
 *
 */
public class CommonRoutes extends Routes {

	@Override
	public void config() {
		add("/fileupload", UploadController.class);
		add("/ext", ExtController.class);
	}

}
