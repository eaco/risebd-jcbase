package com.jcbase.controller.sys;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdProduct;
import com.jcbase.model.RisebdProductType;
import com.jfinal.plugin.activerecord.Page;

public class ProductController extends JCBaseController {
	@RequiresPermissions(value = { "/sys/product" })
	public void index() {
		//主类
		List<RisebdProductType> risebdProductTypes = RisebdProductType.me.getMainProductType();
		StringBuffer names = new StringBuffer();
		StringBuffer ids = new StringBuffer();
		for (RisebdProductType risebdProductType : risebdProductTypes) {
			names.append(risebdProductType.getName() + ",");
			ids.append(risebdProductType.getType() + ",");
		}
		setAttr("names", names);
		setAttr("ids", ids);
		//次类
		List<RisebdProductType> risebdProductTypes2 = RisebdProductType.me.getSecondProductType();
		StringBuffer names2 = new StringBuffer();
		StringBuffer ids2 = new StringBuffer();
		for (RisebdProductType risebdProductType : risebdProductTypes2) {
			names2.append(risebdProductType.getName() + ",");
			ids2.append(risebdProductType.getType() + ",");
		}
		setAttr("names2", names2);
		setAttr("ids2", ids2);
		render("product_index.jsp");
	}

	@RequiresPermissions(value = { "/sys/product/save" })
	public void add() {
		String id = getPara("id");

		List<RisebdProductType> productTypes = RisebdProductType.me.getSecondProductType();
		setAttr("productTypes", productTypes);
		if (id != null) {
			RisebdProduct product = RisebdProduct.me.getById(id);
			setAttr("product", product);
		}
		this.renderJsp("product_add.jsp");
	}

	@RequiresPermissions(value = { "/sys/product/delete" })
	public void delete() {
		String id = getPara("id");
		boolean result = RisebdProduct.me.deleteById(id);
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("code", "success");
		if (result) {
			mapData.put("result", "删除成功！！");
		} else {
			mapData.put("result", "删除失败！！");
		}
		this.renderJson(mapData);
	}

	@RequiresPermissions(value = { "/sys/product/save" })
	public void save() {
		String id = this.getPara("id");
		String name = this.getPara("name");
		Integer second_type = this.getParaToInt("second_type");
		Double org_price = Double.valueOf(this.getPara("org_price"));
		String main_img = this.getPara("main_img");
		Integer is_multi_img = this.getParaToInt("is_multi_img");
		String mark = this.getPara("mark");
		Long orders = this.getParaToLong("orders");
		List<RisebdProductType> risebdProductTypes = RisebdProductType.me.getByType(2, second_type);
		Integer main_type = 0;
		if (null != risebdProductTypes && risebdProductTypes.size() > 0) {
			main_type = risebdProductTypes.get(0).getInt("parent_type");
		}
		is_multi_img = 0;//默认为非多图商品
		int result = RisebdProduct.me.saveProduct(id, name, main_type, second_type, org_price, main_img, is_multi_img,
				orders, mark);
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("code", "success");
		if (result == 1) {
			mapData.put("result", "添加成功！！");
		} else if (result == 2) {
			mapData.put("result", "编辑成功！！");
		}
		this.renderJson(mapData);
	}

	/**
	 * @category 列表
	 */
	public void getListData() {
		String name = this.getPara("name");
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(name)) {
			conditions.add(new Condition("name", Operators.LIKE, name));
		}
		Page<RisebdProduct> pageInfo = RisebdProduct.me.getPage(getPage(), this.getRows(), conditions,
				this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}
}
