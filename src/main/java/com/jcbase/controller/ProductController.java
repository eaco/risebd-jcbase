package com.jcbase.controller;

import java.util.List;

import com.jcbase.core.auth.interceptor.AuthorityInterceptor;
import com.jcbase.core.auth.interceptor.SysLogInterceptor;
import com.jcbase.model.RisebdProduct;
import com.jcbase.model.RisebdProductType;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;

/**
 * @category 产品管理
 * 
 * @author yiz
 * @date 2016年9月12日 下午4:27:31
 * @version 1.0.0 
 */
@Clear({ AuthorityInterceptor.class, SysLogInterceptor.class })
public class ProductController extends Controller {

	public void index() {
		//得到主类Id
		int mainType = getParaToInt(0);
		List<RisebdProductType> productTypes = RisebdProductType.me.getProductTypeByMainType(mainType);
		for (RisebdProductType productType : productTypes) {
			List<RisebdProduct> products = RisebdProduct.me.getRisebdProductsByTypes(productType.getParentType(),
					productType.getType());
			productType.setRisebdProducts(products);
		}
		setAttr("productTypes", productTypes);
		render("product.jsp");
	}

}
