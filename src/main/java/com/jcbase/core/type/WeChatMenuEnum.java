package com.jcbase.core.type;

/**
 * @category 微信菜单
 * @author yiz
 *
 */
public enum WeChatMenuEnum {
	/**
	 * @category 微信相册发图
	 */
	pic_weixin,
	/**
	 * @category 拍照或者相册发图
	 */
	pic_photo_or_album,
	/**
	 * @category 系统拍照发图
	 */
	pic_sysphoto,
	/**
	 * @category 扫码推事件
	 */
	scancode_push,
	/**
	 * @category 连接
	 */
	view,
	/**
	 * @category 发送位置
	 */
	location_select,
	/**
	 * @category 点击事件
	 */
	click
}
