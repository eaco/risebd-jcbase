package com.jcbase.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcbase.core.BaseConstance;
import com.jcbase.core.auth.interceptor.AuthorityInterceptor;
import com.jcbase.core.auth.interceptor.SysLogInterceptor;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.weixin.interceptor.Oauth2Interceptor;
import com.jcbase.model.RisebdCity;
import com.jcbase.model.RisebdLink;
import com.jcbase.model.RisebdProductType;
import com.jcbase.service.CacheService;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;

@Clear({ AuthorityInterceptor.class, SysLogInterceptor.class })
@Before(Oauth2Interceptor.class)
public class IndexController extends JCBaseController {
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

	public void index() {
		setCookie(BaseConstance.MEMBERID_COOKIE_KEY, "yizhao", 24 * 10 * 3600);
		setCookie(BaseConstance.OPENID_COOKIE_KEY, "xxxxxxxxxxxx", 24 * 10 * 3600);
		//得到大类
		List<RisebdProductType> productTypes = RisebdProductType.me.getMainProductType();
		//得到首页滚动条信息
		List<RisebdLink> bannerlinks = RisebdLink.me.getLinksByType(1);
		List<RisebdLink> helpLinks = RisebdLink.me.getLinksByType(3);
		String memberId = getCookie(BaseConstance.MEMBERID_COOKIE_KEY);
		//得到购物车数量
		Integer carNum = CacheService.getShopCarNum(memberId);
		//得到当前切换的城市
		String currentCity = getCookie(BaseConstance.CITY_COOKIE_KEY);
		//得到城市列表
		List<RisebdCity> risebdCities = RisebdCity.me.getAllAvailableCity();
		if (CommonUtils.isEmpty(currentCity) && null != risebdCities && risebdCities.size() > 0) {
			currentCity = risebdCities.get(0).getCity();
			try {
				currentCity = URLEncoder.encode(currentCity, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error("异常e={}", e);
			}
			setCookie(BaseConstance.CITY_COOKIE_KEY, currentCity, 10 * 24 * 2600);
		}
		if (CommonUtils.isNotEmpty(currentCity)) {
			try {
				currentCity = URLDecoder.decode(currentCity, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error("异常e={}", e);
			}
		}
		setAttr("productTypes", productTypes);
		setAttr("bannerlinks", bannerlinks);
		setAttr("helpLinks", helpLinks);
		setAttr("risebdCities", risebdCities);
		setAttr("currentCity", currentCity);
		setAttr("carNum", carNum);
		render("index.jsp");
	}

	public void changeCity() {
		String city = getPara("city");
		try {
			city = URLEncoder.encode(city, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("异常e={}", e);
		}
		setCookie(BaseConstance.CITY_COOKIE_KEY, city, 10 * 24 * 3600);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		renderJson(map);
	}

}
