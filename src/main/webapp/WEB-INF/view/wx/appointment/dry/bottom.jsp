<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
</head>
<body>
<div class="weui_tabbar">
        <a href="/" class="weui_tabbar_item weui_bar_item_on" onclick="addOn(this)">
            <div class="weui_tabbar_icon">
                <img src="${res_url}css/appointment/images/icon_nav_button.png" alt="">
            </div>
            <p class="weui_tabbar_label">首页</p>
        </a>
        <a href="/appointment/list" class="weui_tabbar_item" onclick="addOn(this)">
            <div class="weui_tabbar_icon">
                <img src="${res_url}css/appointment/images/icon_nav_article.png" alt="">
            </div>
            <p class="weui_tabbar_label">订单</p>
        </a>
        <a href="/uc/me" class="weui_tabbar_item" onclick="addOn(this)">
            <div class="weui_tabbar_icon">
                <img src="${res_url}css/appointment/images/icon_nav_cell.png" alt="">
            </div>
            <p class="weui_tabbar_label">我</p>
        </a>
</div>
<script type="text/javascript">
	function addOn(obj){
		$(obj).addClass('weui_bar_item_on').siblings('.weui_bar_item_on').removeClass('weui_bar_item_on');
	}
</script>
</body>
</html>