package com.jcbase.controller;

import com.jcbase.core.auth.interceptor.AuthorityInterceptor;
import com.jcbase.core.auth.interceptor.SysLogInterceptor;
import com.jfinal.aop.Clear;

/**
 * @category 订单处理
 * 
 * @author yiz
 * @date 2016年8月10日 下午3:34:30
 * @version 1.0.0 
 * @copyright pycredit.cn 
 */
@Clear({ AuthorityInterceptor.class, SysLogInterceptor.class })
public class OrderController {

}
