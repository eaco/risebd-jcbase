package com.jcbase.controller.sys;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdCoupon;
import com.jcbase.model.RisebdCouponRule;
import com.jcbase.model.RisebdMember;
import com.jfinal.plugin.activerecord.Page;

/**
 * @category 优惠券
 * 
 * @author yiz
 * @date 2016年8月10日 下午5:25:35
 * @version 1.0.0 
 * @copyright 
 */
public class CouponController extends JCBaseController {
	@RequiresPermissions(value = { "/sys/coupon" })
	public void index() {
		this.renderJsp("coupon_index.jsp");
	}

	public void getListData() {
		String couponNo = this.getPara("couponNo");
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(couponNo)) {
			conditions.add(new Condition("coupon_no", Operators.EQ, couponNo));
		}
		conditions.add(new Condition("status", Operators.NOT_EQ, 5));
		Page<RisebdCoupon> pageInfo = RisebdCoupon.me.getPage(getPage(), this.getRows(), conditions, this.getOrderby());
		List<RisebdCoupon> risebdCoupons = pageInfo.getList();
		for (RisebdCoupon risebdCoupon : risebdCoupons) {
			RisebdCouponRule risebdCouponRule = RisebdCouponRule.me.getById(risebdCoupon.getRuleId());
			if (null != risebdCouponRule) {
				risebdCoupon.put("risebdCouponRule", risebdCouponRule);
			}
			if (CommonUtils.isNotEmpty(risebdCoupon.getMemberId())) {
				RisebdMember risebdMember = RisebdMember.me.getById(risebdCoupon.getMemberId());
				risebdCoupon.put("risebdMember", risebdMember);
			}
		}
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}
}
