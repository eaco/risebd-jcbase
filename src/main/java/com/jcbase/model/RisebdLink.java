package com.jcbase.model;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.jcbase.core.util.StringUtils;
import com.jcbase.core.util.UUIDHexgenerator;
import com.jcbase.model.base.BaseRisebdLink;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class RisebdLink extends BaseRisebdLink<RisebdLink> {
	public static final RisebdLink me = new RisebdLink();

	public RisebdLink getById(String id) {
		return findById(id);
	}
	
	public boolean delById(String id){
		return deleteById(id);
	}
	
	public List<RisebdLink> getLinksByType(Integer type){
		return this.find(" select * from "+getTableName()+" where type=? order by orders ",type);
	}

	public int saveLink(String id, String name, String link, String coverImg, Integer type, Integer orders) {
		Integer result = 0;
		if (StringUtils.isBlank(id)) {
			RisebdLink risebdLink = new RisebdLink();
			risebdLink.set("id", UUIDHexgenerator.generate()).set("name", name).set("link", link).set("type", type)
					.set("orders", orders).set("cover_img", coverImg)
					.set("create_time", new Timestamp(Calendar.getInstance().getTimeInMillis()))
					.set("update_time", new Timestamp(Calendar.getInstance().getTimeInMillis()));
			boolean flag = risebdLink.save();
			if (flag) {
				result = 1;
			}
		} else {
			RisebdLink risebdLink = getById(id);
			risebdLink.set("name", name).set("link", link).set("type", type).set("cover_img", coverImg)
					.set("orders", orders).set("update_time", new Date());
			boolean flag = risebdLink.update();
			if (flag) {
				result = 2;
			}
		}
		return result;
	}

}
