<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<jsp:include page="../css.jsp" flush="true" />
<link href="${res_url}js/area-select/css/select2.css" rel="stylesheet"/>
</head>

<body>
	<div class="main">
		<div class="editAddress">
				<ul class="inputArea">
					<li class="relative">
						<input type="hidden" name="id" id="id" value="${memberAddress.id}">
						<input type="hidden" name="moduleType" id="moduleType" value="${moduleType}">
						<input type="hidden" name="currentCity" id="currentCity" value="${currentCity}">
						<input type="hidden" name="is_default" id="is_default" value="${memberAddress.isDefault}">
						<input type="hidden" name="k_province" id="k_province" value="${memberAddress.k_province}">
						<input type="hidden" name="k_city" id="k_city" value="${memberAddress.k_city}">
						<input type="hidden" name="k_area" id="k_area" value="${memberAddress.k_area}">
						
						<input type="text" maxlength="11" size="11" placeholder="请输入手机号" onkeyup='this.value=this.value.replace(/\D/gi,"")'  name="mobile" id="mobile" value="${memberAddress.mobile}">
					</li>
					<li>
						<input type="text" placeholder="请输入姓名" name="name" id="name" value="${memberAddress.username }">
					</li>
					<li>
						<select id="loc_province" style="width:100%;padding-left: 0.35714rem">
						</select>
					</li>
					<li>
						<select id="loc_city"  style="width:100%;padding-left: 0.35714rem">
						</select>
					</li>
					<li>
						<select id="loc_town" style="width:100%;padding-left: 0.35714rem">
						</select>
					</li>
					<%-- <li class="rArrow">
						<a href="javascript:void(0);" onclick="choosecity()">
							<input type="text" placeholder="选择门店" name="store_id" id="store_id" readonly="readonly" value="${store_name }">
						</a>
					</li> --%>
					<li>
						<input type="text" placeholder="详细地址，请输入小区名楼栋号以及房号" name="address" id="address" value="${memberAddress.address }">
					</li>
				</ul>
				<div class="btnWrap">
					<button  class="saveBtn" onclick="doEditMemberAddress()">保存</button>
				</div>
		</div>
	</div>
	<jsp:include page="../js.jsp" flush="true" />
	<script src="${res_url}js/area-select/js/jquery.js"></script>
	<script type="text/javascript" src="${res_url}js/area-select/js/area.js"></script>
	<script type="text/javascript" src="${res_url}js/area-select/js/location.js"></script>
	<script type="text/javascript" src="${res_url}js/area-select/js/select2.js"></script>
	<script type="text/javascript" src="${res_url}js/area-select/js/select2_locale_zh-CN.js"></script>
	<script type="text/javascript" src="${res_url}js/appointment/address.js"></script>
	
</body>

</html>