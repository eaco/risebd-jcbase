/**
 * 
 */

function beginPay(appointId){
	//选择的情况
	var couponNo = $("#couponSelect").val();
	if(couponNo == '0'){
		couponNo ='';
	}
	var payType = $("input[name='paySelect']:checked").val(); 
	if(null == payType || undefined == payType){
		alert("请选择支付方式");
		return;
	}
	var openid = $("#openid").val();
	var params ={};
	params['couponNo'] = couponNo;
	params['appointId']=appointId;
	params['payType']=payType;
	params['openid']=openid;
	if(payType== 1){
		//校验余额
		$.post('/pay/appointmentYePay',params,function(data){
			 if(data.code == 1){
            	layer.msg(data.message, {
					icon : 2,
					time : 2000
				// 2秒关闭（如果不配置，默认是3秒）
				});
	            return;
            }else{
            	layer.msg(data.message, {
					icon : 1,
					time : 2000
				// 2秒关闭（如果不配置，默认是3秒）
				});
            }
		});
	}else if(payType == 2){
		//开始调用微信支付
		var pasToken='';
		 $.ajax({
			  	url:'/pay/appointmentWePay',
				type:"post",
				dataType:"json",
				async:false,
				data:{"openid":openid,"appointId":appointId,'couponNo':couponNo},
				success:function(data){
		            if(data.code == 1){
		            	layer.msg(data.message, {
							icon : 2,
							time : 2000
						// 2秒关闭（如果不配置，默认是3秒）
						});
			            return;
		            }
		            //alert("a:"+data.timeStamp+"---b:"+data.nonceStr+"---c:"+data.packageValue+"---d:"+data.signType+"---e:"+data.paySign+"----f:"+data.appId);
					wx.chooseWXPay({
						timestamp: data.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
					    nonceStr: data.nonceStr, // 支付签名随机串，不长于 32 位
					    "package": data.packageValue, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
					    signType: data.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
					    paySign: data.paySign, // 支付签名
					    success: function (res) {
					    	layer.msg('支付成功!', {
								icon : 1,
								time : 2000
							// 2秒关闭（如果不配置，默认是3秒）
							});
					    	window.location.href="/appointment/list";
					    },complete: function (res) {
					        //alert(JSON.stringify(res));
					     },
					     cancel: function (res) {
					       // alert(JSON.stringify(res));
					      },
					    fail: function (res) {
					        alert(JSON.stringify(res));
					      }
					});
				},
				fail:function(data){
					return;
					}
		 });
	}else{
		//支付失败
		layer.msg('支付失败!', {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		window.location.href='/appointment/list';
	}
}