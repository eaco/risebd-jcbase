<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<jsp:include page="/WEB-INF/view/common/basecss.jsp" flush="true" />
</head>

<body class="no-skin">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<div class="main-content" style="margin-left: 0px;">
					<div class="page-content">
						<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<form class="form-horizontal" id="validation-form" method="post">
								<div class="form-group">
									<input name="id" type="hidden" value="${risebdArticle.id}"/>
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="title">标题</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
								            <input type="text" name="title" id="title" value="${risebdArticle.title}" class="col-xs-12 col-sm-6">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="author">作者</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
								            <input type="text" name="author" id="author" value="${risebdArticle.author}" class="col-xs-12 col-sm-6">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="author">封面</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
											<input id="fileupload" type="file" name="files[]" data-url="/fileupload/uploadImg"  multiple >
											<img alt="" src="${ static_url}${risebdArticle.coverImg }" id="img" width="50px" onerror="javascript:this.src='${pageContext.request.contextPath}/res/img/default.jpg'">
								            <input type="hidden" name="coverImg" id="coverImg" value="${risebdArticle.coverImg}" >
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">排序</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
								            <input type="text" name="orders" id="orders" value="${risebdArticle.orders}" class="col-xs-12 col-sm-6">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="url">类型</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
											<select name="type" id="type" class="col-xs-12 col-sm-6">
												<option value="1">公告</option>
												<option value="2">新闻</option>
												<option value="3">文章</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">状态</label>
									<div class="col-xs-12 col-sm-9">
										<div class="clearfix">
											<select name="status" id="status" class="col-xs-12 col-sm-6">
												<option value="0">草稿</option>
												<option value="1">发布</option>
												<option value="2">下架</option>
												<option value="3">禁用</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="orders">内容</label>
								</div>
								<textarea id="text" name="text" style="padding-left:80px" >${risebdArticle.text }</textarea>
								<div class="clearfix form-actions" align="center">
									<div class="col-md-offset-3 col-md-9">
										<button id="submit-btn" class="btn btn-info" type="submit" data-last="Finish">
											<i class="ace-icon fa fa-check bigger-110"></i>
											提交
										</button>
										&nbsp; &nbsp; &nbsp;
										<button class="btn" type="reset">
											<i class="ace-icon fa fa-undo bigger-110"></i>
											重置
										</button>
									</div>
								</div>
								
								
							</form>
						</div><!-- /.col -->
					</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
		</div><!-- /.main-container -->
			<!-- basic scripts -->
<jsp:include page="/WEB-INF/view/common/basejs.jsp" flush="true" />
    <script type="text/javascript" charset="utf-8" src="/res/ueditor/1.4/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/res/ueditor/1.4/ueditor.all.min.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="/res/ueditor/1.4/lang/zh-cn/zh-cn.js"></script>

	<script type="text/javascript">
			$(document).ready(function(){
				initformSubmitEvent();
				defaultSelect("type","${risebdArticle.type}");
				defaultSelect("status","${risebdArticle.status}");
				//初始化编辑器
				var ue = UE.getEditor('text',{
					 toolbars: [[
					              'fullscreen', 'source', '|', 'undo', 'redo', '|',
					              'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 
					              'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 
					              'insertunorderedlist', 'selectall', 'cleardoc', '|',
					              'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
					              'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
					              'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', 
					              'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
					              'simpleupload', 'insertimage', 'emotion', 'scrawl', 'insertvideo', 'music', 'attachment', 'map',  'insertcode', 
					              'print', 'preview',
					          ]]
					,initialFrameWidth:680  //初始化编辑器宽度,默认800
					          ,initialFrameHeight:320  //初始化编辑器高度,默认320
				});
				$('#fileupload').fileupload({
				    done: function (e, data) {//设置文件上传完毕事件的回调函数  
				    	var result = data.result;
				    	if(result.code==0){
				    		layer.alert("上传文件失败", 9, !1);
				    		return false;
				    	}else if(result.code==1){
				    		$("#img").attr("src",result.staticUrl+result.fileUrl);
				    		$("#coverImg").val(result.fileUrl);
				    	}
				    }
				});
			});
			var setting = {
			view: {
				dblClickExpand: false
			},
			data: {
				simpleData: {
				enable: true
			}
			},
			callback: {
				beforeClick: beforeClick,
				onClick: onClick
			}
			};
			
			//json数据源，也可以从后台读取json字符串，并转换成json对象，如下所示
			var strNodes = '${jsonTree}';
			var zNodes = eval("("+strNodes+")"); //将json字符串转换成json对象数组，strNode一定要加"（）"，不然转不成功
			function beforeClick(treeId, treeNode) {
				var check = (treeNode.id!=10000);
			if (!check) alert("这是标题，不能选！");
				return check;
			}
			function onClick(e, treeId, treeNode) {
				$("#parent_id").val(treeNode.id);
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getSelectedNodes(),
				v = "";
				nodes.sort(function compare(a,b){return a.id-b.id;});
			for (var i=0, l=nodes.length; i<l; i++) {
				v += nodes[i].name + ",";
			}
				if (v.length > 0 ) v = v.substring(0, v.length-1);
				var cityObj = $("#parent_name");
				cityObj.attr("value", v); 
			}
			
			function showMenu() {
				var cityObj = $("#parent_name");
				var cityOffset = $("#parent_name").offset();
				var sel=document.getElementById("kka");
				$("#menuContent").css({left:sel.offsetLeft + "px", top:sel.offsetTop + cityObj.outerHeight() + "px"}).slideDown("fast");
				$("body").bind("mousedown", onBodyDown);
			}
			function hideMenu() {
				$("#menuContent").fadeOut("fast");
				$("body").unbind("mousedown", onBodyDown);
			}
			function onBodyDown(event) {
				if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
					hideMenu();
				}
				if(event.target.id.length>=15&&event.target.id.length!=17){
					hideMenu();
				}
			}
			function defaultSelect(id,value){
				//默认选中
				if(value){
					$("#"+id).val(value);
				}
			}
			
			var $validation = true;
			function initformSubmitEvent(){
				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					rules: {
						title:{
							required: true
						},
						author:{
							required: true
						},
						orders:{
							required: true,
							digits:true
						}
					},
					messages: {
						title:{
							required: "请输入标题"
						},
						author:{
							required: "请输入作者"
						},
						orders:{
							required: "请输入排序号",
							digits:"排序号只能输入整数"
						}
					},
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
						var $form = $("#validation-form");
						var $btn = $("#submit-btn");
						if($btn.hasClass("disabled")) return;
						$btn.addClass("disabled");
						 var postData=$("#validation-form").serialize();
			        	 $.post("/sys/article/save" ,postData,function(data){
			        		if(data.code=='success'){
			        			layer.msg('操作成功', {
			        				icon: 1,
			        			    time: 2000 //2秒关闭（如果不配置，默认是3秒）
			        			},function(){
			        				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			        				parent.reloadGrid();
			        				parent.layer.close(index); //再执行关闭 
			        			});
			        		}
			        	},"json");
		        		$("#submit-btn").removeClass("disabled");
						return false;
					},
					invalidHandler: function (form) {
					}
				});
			}
		</script>
</body>

</html>

