<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<jsp:include page="../css.jsp" flush="true" />
</head>

<body>
	<div class="main makeAppointment">
		<h2 class="appointmentHeader">请填写预约详情:</h2>
		<div class="appointmentBody">
				<div class="consigneeInfo">

					<div class="itemFirst dateTimeSelect">
						<div class="date ">
							<input placeholder="请选择服务日期" type="text" onfocus="(this.type='date')" id="date">
						</div>
						<div class="time ">
							<input placeholder="请选择服务时间" type="text" onfocus="(this.type='time')" id="time">
						</div>
					</div>
					<input type="hidden" value="${memberAddress.username }" id="name">
					<input type="hidden" value="${memberAddress.mobile }" id="mobile">
					<input type="hidden" value="${memberAddress.province}" id="province">
					<input type="hidden" value="${memberAddress.city}" id="city">
					<input type="hidden" value="${memberAddress.area}" id="area">
					<input type="hidden" value="${memberAddress.address }" id="address">
					<c:if test="${!empty memberAddress }">
						<div class="itemSecond userInfo">
							<p>
							<span class="nick">${memberAddress.username }</span>
							<span class="phone">${memberAddress.mobile }</span></p>
							<p class="address">${memberAddress.province}${memberAddress.city}${memberAddress.area}${memberAddress.address }</p>
							<a href="/uc/editaddress?id=${memberAddress.id }&moduleType=3" class="editLink">修改</a>
						</div>
					</c:if>
					<c:if test="${empty memberAddress }">
						<div class="itemSecond userInfo">
						<a href="/uc/editaddress?id=&moduleType=3" class="editLink">请选择预约取货地址</a>
						</div>
					</c:if>
					<div class="remarks">
						<textarea name="description" id="description" placeholder="备注预约信息，如是否需要袋子或包装纸盒,若有疑问可咨询客服人员。"></textarea>
					</div>
					<div class="btnWrap">
						<button type="button" class="btn appointmentBtn" onclick="doSimpleAppointment()">预约</button>
			
					</div>
				</div>

		</div>
	</div>

<jsp:include page="../js.jsp" flush="true" />
<script type="text/javascript" src="${res_url}js/appointment/appointment.js"></script>
</body>
</html>