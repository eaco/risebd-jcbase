package com.jcbase.dto;

import java.io.Serializable;

/**
 * 支付附加参数
 * 
 * @author yiz
 * @date 2016年10月31日 上午9:35:10
 * @version 1.0.0 
 */
public class PayAttach implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4271703848628873662L;
	/**
	 * openId
	 */
	private String openId;
	/**
	 * 预约号
	 */
	private String appointId;
	/**
	 * 订单支付总额
	 */
	private String total_fee;

	public PayAttach(String openId, String appointId, String total_fee) {
		this.openId = openId;
		this.appointId = appointId;
		this.total_fee = total_fee;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getAppointId() {
		return appointId;
	}

	public void setAppointId(String appointId) {
		this.appointId = appointId;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

}
