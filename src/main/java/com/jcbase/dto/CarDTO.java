package com.jcbase.dto;

import java.io.Serializable;

/**
 * 购物车实体
 * 
 * @author yiz
 * @date 2016年10月29日 上午9:29:10
 * @version 1.0.0 
 */
public class CarDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4380642912852150369L;

	/**
	 * token
	 */
	private Long aaToken;

	private double originaPrice;
	/**
	 * @category 现价
	 */
	private double nowPrice = 0;

	/**
	 * @category 价格标题
	 */
	private String priceTitle;

	/**
	 * @category 图片
	 */
	private String mainImg;

	/**
	 * 产品名称
	 */
	private String productName;

	/**
	 * 产品主类
	 */
	private String productMainTypeName;

	/**
	 * 产品次类
	 */
	private String productSecondTypeName;

	/**
	 * @category 数量
	 */
	private Integer num = 1;

	public Long getAaToken() {
		return aaToken;
	}

	public void setAaToken(Long aaToken) {
		this.aaToken = aaToken;
	}

	public double getOriginaPrice() {
		return originaPrice;
	}

	public void setOriginaPrice(double originaPrice) {
		this.originaPrice = originaPrice;
	}

	public double getNowPrice() {
		return nowPrice;
	}

	public void setNowPrice(double nowPrice) {
		this.nowPrice = nowPrice;
	}

	public String getPriceTitle() {
		return priceTitle;
	}

	public void setPriceTitle(String priceTitle) {
		this.priceTitle = priceTitle;
	}

	public String getMainImg() {
		return mainImg;
	}

	public void setMainImg(String mainImg) {
		this.mainImg = mainImg;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductMainTypeName() {
		return productMainTypeName;
	}

	public void setProductMainTypeName(String productMainTypeName) {
		this.productMainTypeName = productMainTypeName;
	}

	public String getProductSecondTypeName() {
		return productSecondTypeName;
	}

	public void setProductSecondTypeName(String productSecondTypeName) {
		this.productSecondTypeName = productSecondTypeName;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

}
