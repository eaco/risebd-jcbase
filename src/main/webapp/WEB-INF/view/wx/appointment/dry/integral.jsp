<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<%
	String contextPath = request.getContextPath();
%>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content=",initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>我的积分</title>
    <!-- inject:css -->
    <link rel="stylesheet" href="<%=contextPath %>/resources/css/main.css">
    <!-- endinject -->

    <body>
        <div class="main integralWrap">
            <div class="totalWrap">
                <div class="integralTotal">
                    <span>积分</span>
                    <span>${member.integral }</span>
                </div>
                <p class="title">积分明细</p>
            </div>
            <ul class="integralDetail">
            	<c:forEach items="${memberIntegals }" var="memberIntegal">
            		<li>${memberIntegal.typeName}<span>+<i>${memberIntegal.integral }</i></span></li>
            	</c:forEach>
            </ul>
        </div>
    </body>

</html>