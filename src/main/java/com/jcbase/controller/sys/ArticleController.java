package com.jcbase.controller.sys;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdArticle;
import com.jfinal.plugin.activerecord.Page;

/**
 * @category 内容控制器
 * @author yiz
 *
 */
public class ArticleController extends JCBaseController {

	@RequiresPermissions(value = { "/sys/article" })
	public void index() {
		render("article_index.jsp");
	}

	@RequiresPermissions(value = { "/sys/article/save" })
	public void add() {
		String id = getPara("id");
		if (id != null) {
			RisebdArticle risebdArticle = RisebdArticle.me.getById(id);
			setAttr("risebdArticle", risebdArticle);
		}
		this.renderJsp("article_add.jsp");
	}

	@RequiresPermissions(value = { "/sys/article/save" })
	public void save() {
		String id = getPara("id");
		String title = getPara("title");
		String author = getPara("author");
		String coverImg = getPara("coverImg");
		Integer status = getParaToInt("status");
		Integer type = getParaToInt("type");
		Integer orders = getParaToInt("orders");
		String text = getPara("text");
		int result = RisebdArticle.me.saveArticle(id, title, author, coverImg, status, type, orders, text);
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("code", "success");
		if (result == 1) {
			mapData.put("result", "添加成功！！");
		} else if (result == 2) {
			mapData.put("result", "编辑成功！！");
		}
		this.renderJson(mapData);
	}

	@RequiresPermissions(value = { "/sys/article/save" })
	public void updateStatus() {
		String id = getPara("id");
		Integer status = getParaToInt("status");
		int result = RisebdArticle.me.updateArticleStatus(id, status);
		Map<String, Object> mapData = new HashMap<String, Object>();
		if (1 == result) {
			mapData.put("code", "success");
			mapData.put("result", "更新状态成功！！");
		} else {
			mapData.put("code", "fail");
			mapData.put("result", "更新状态失败！！");

		}
		this.renderJson(mapData);
	}

	/**
	 * @category 列表
	 */
	public void getListData() {
		String title = this.getPara("title");
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(title)) {
			conditions.add(new Condition("title", Operators.LIKE, title));
		}
		conditions.add(new Condition("status", Operators.NOT_IN, 3));
		Page<RisebdArticle> pageInfo = RisebdArticle.me.getPage(getPage(), this.getRows(), conditions,
				this.getOrderby(), "id", "type", "title", "author", "status", "cover_img", "orders", "create_time",
				"update_time");
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}
}
