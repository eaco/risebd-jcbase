package com.jcbase.controller;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import com.jcbase.core.BaseConstance;
import com.jcbase.core.auth.interceptor.AuthorityInterceptor;
import com.jcbase.core.auth.interceptor.SysLogInterceptor;
import com.jcbase.core.util.DateUtils;
import com.jcbase.model.RisebdAppointTaking;
import com.jcbase.model.RisebdMemberAddress;
import com.jcbase.model.RisebdMemberAppoint;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;

/**
 * @category 预约管理
 * @author yiz
 *
 */
@Clear({ AuthorityInterceptor.class, SysLogInterceptor.class })
public class AppointmentController extends Controller {

	/**
	 * @category 个人预约中心
	 */
	public void index() {

	}

	/**
	 * @category 一键预约
	 */
	public void simple() {
		String memberAddressId = getPara("memberAddressId");//得到地址id
		String memberid = getCookie(BaseConstance.MEMBERID_COOKIE_KEY);
		RisebdMemberAddress memberAddress = RisebdMemberAddress.me.getById(memberid, memberAddressId);
		setAttr("memberAddress", memberAddress);
		render("simple.jsp");
	}

	public void list() {
		String memberid = getCookie(BaseConstance.MEMBERID_COOKIE_KEY);
		Integer appointType = getParaToInt(0);
		String limitTime = DateUtils.format(DateUtils.addDay(DateUtils.getNowDateTime(), -100), "yyyy-MM-dd");
		List<RisebdMemberAppoint> memberAppoints = RisebdMemberAppoint.me.list(memberid, appointType, limitTime);
		for (RisebdMemberAppoint memberAppoint : memberAppoints) {
			RisebdAppointTaking risebdAppointTaking = RisebdAppointTaking.me.getByAppointId(memberAppoint.getId());
			memberAppoint.setRisebdAppointTaking(risebdAppointTaking);
		}
		setAttr("memberAppoints", memberAppoints);
		setAttr("appointType", appointType);
		render("appointmentList.jsp");
	}

	public void doSimpleAppointment() {
		String memberid = getCookie(BaseConstance.MEMBERID_COOKIE_KEY);
		String name = getPara("name");
		String mobile = getPara("mobile");
		String province = getPara("province");
		String city = getPara("city");
		String area = getPara("area");
		String address = getPara("address");
		String reservationDate = getPara("reservationDate");
		String reservationTime = getPara("reservationTime");
		String description = getPara("description");
		String id = getPara("id");
		int result = RisebdMemberAppoint.me.saveAppointment(id, 0, 0, 0, mobile, name, null, null,
				province + city + area + address, reservationDate + " " + reservationTime, null, null, null, memberid,
				null, description);
		Map<String, Object> mapData = Maps.newHashMap();
		if (result > 0) {
			mapData.put("code", "success");
			mapData.put("result", "预约成功");
		} else {
			mapData.put("code", "fail");
			mapData.put("result", "预约失败");
		}
		renderJson(mapData);
	}
}
