<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<jsp:include page="../css.jsp" flush="true" />
</head>

<body>
	<div class="main">
		<div class="shoppingcarBtn">
				<a href="/shoppingcar/shoppingcar">
					<i></i>
					<span>${carNum }</span>
				</a>
			</div>
		<div>
		<div class="clothesWrap changeCr">
			<ul class="countTypeNav">
				<li class="piece current">按件计价</li>
				<li class="package">按袋计价</li>
			</ul>

			<form action="">
				<div class="countBody">


					<ul class="priceTypeNav changeNav">
						<li class="current"><i class="RMB">￥</i><i class="price">9</i>/件</li>
						<li><i class="RMB">￥</i><i class="price">19</i>/件</li>
						<li><i class="RMB">￥</i><i class="price">29</i>/件</li>
					</ul>
					<div class="priceTypeBody">


						<div class="nineClothes priceTypeContent changeCt">
							<p class="typeDesc">夏装(衬衫、T恤、所有裤装、长短裙等轻薄款，含：领带、围巾等配件）</p>
							<ul class="clothesType cleanList">
								<c:forEach items="${priceDTOs_9 }" var="priceDTO" varStatus="i">
									<li onclick="addToCar(${priceDTO.id},'${openid }','${priceDTO.priceNow }','${priceDTO.priceTitle }','${priceDTO.priceProductImg }','${priceDTO.category }','caItem${i.index}',1);">
										<input type="checkbox" id="caItem${i.index }">
										<label for="caItem${i.index }">
											<img src="${priceDTO.priceProductImg }" alt="">
											<span>${priceDTO.priceTitle }</span>
										</label>
	
									</li>
								</c:forEach>
							</ul>
						</div>
						<div class="nineteenClothes priceTypeContent changeCt">
							<p class="typeDesc">春秋装（西装、夹克等普通外套、连衣裙）</p>
							<ul class="clothesType cleanList">
								<c:forEach items="${priceDTOs_19 }" var="priceDTO" varStatus="i">
									<li onclick="addToCar(${priceDTO.id},'${openid }','${priceDTO.priceNow }','${priceDTO.priceTitle }','${priceDTO.priceProductImg }','${priceDTO.category }','cbItem${i.index}',1);">
										<input type="checkbox" id="cbItem${i.index}">
										<label for="cbItem${i.index}">
											<img src="${priceDTO.priceProductImg }" alt="">
											<span>${priceDTO.priceTitle }</span>
										</label>
									</li>
								</c:forEach>
							</ul>
						</div>
						<div class="twentyNineClothes priceTypeContent changeCt">
							<p class="typeDesc">冬装大衣外套（羽绒服、棉服、羊毛羊绒外套等，含：旗袍）；皮衣清洗160元/件</p>
							<ul class="clothesType cleanList">
								<c:forEach items="${priceDTOs_29 }" var="priceDTO" varStatus="i">
									<li onclick="addToCar(${priceDTO.id},'${openid }','${priceDTO.priceNow }','${priceDTO.priceTitle }','${priceDTO.priceProductImg }','${priceDTO.category }','ccItem${i.index}',1);">
										<input type="checkbox" id="ccItem${i.index}">
										<label for="ccItem${i.index}">
											<img src="${priceDTO.priceProductImg }" alt="">
											<span>${priceDTO.priceTitle }</span>
										</label>
									</li>
								</c:forEach>

							</ul>
						</div>
					</div>

				</div>
				<div class="countBody">
					<div class="priceTypeBody">
						<div class="nineClothes priceTypeContent changeCt">
							<ul class="clothesType cleanList">
								<c:forEach items="${priceDTOs_package }" var="priceDTO" varStatus="i">
									<li onclick="addToCar(${priceDTO.id},'${openid }','${priceDTO.priceNow }','${priceDTO.priceTitle }','${priceDTO.priceProductImg }','${priceDTO.category }','cgItem${i.index}',1);">
										<input type="checkbox" id="cgItem${i.index }">
										<label for="cgItem${i.index }">
											<img src="${priceDTO.priceProductImg }" alt="">
											<span>${priceDTO.priceTitle }</span>
										</label>
	
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<div class="packageCount">
						<div class="cantWrap">
							<h2 class="title">不能装</h2>
							<ul class="cantList">
								<li>
									<p><i></i><span>鞋</span></p>
								</li>
								<li>
									<p><i></i><span>包</span></p>
								</li>
								<li>
									<p><i></i><span>皮衣</span></p>
								</li>
								<li>
									<p><i></i><span>医用衣物</span></p>
								</li>
								<li>
									<p><i></i><span>婚纱</span></p>
								</li>
								<li>
									<p><i></i><span>窗帘</span></p>
								</li>

							</ul>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<jsp:include page="../js.jsp" flush="true" />
</body>

</html>