package com.jcbase.controller.sys;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdWechatReply;
import com.jfinal.plugin.activerecord.Page;

public class WechatReplyController extends JCBaseController {
	private Logger logger = Logger.getLogger(getClass());

	@RequiresPermissions(value = { "/sys/wechat/reply" })
	public void index() {
		render("wechat_reply_index.jsp");
	}

	@RequiresPermissions(value = { "/sys/wechat/reply" })
	public void add() {
		String id = getPara("id");
		logger.info("请求参数id=" + id);
		if (id != null) {
			RisebdWechatReply risebdWechatReply = RisebdWechatReply.me.getById(id);
			setAttr("wechatReply", risebdWechatReply);
		}
		this.renderJsp("wechat_reply_add.jsp");
	}

	public void getListData() {
		String keyword = this.getPara("keyword");
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(keyword)) {
			conditions.add(new Condition("keyword", Operators.LIKE, keyword));
		}
		Page<RisebdWechatReply> pageInfo = RisebdWechatReply.me.getPage(getPage(), this.getRows(), conditions,
				this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}

	@RequiresPermissions(value = { "/sys/wechat/reply" })
	public void saveRes() {
		String id = getPara("id");
		String keyword = getPara("keyword");
		String content = getPara("content");
		Integer type = getParaToInt("type");
		String title = getPara("title");
		Integer msg_type = getParaToInt("msg_type");
		String main_img = getPara("main_img");
		String url = getPara("url");
		List<RisebdWechatReply> risebdWechatReplies = RisebdWechatReply.me.getByTypeAndKey(type, keyword);
		int size = risebdWechatReplies.size();
		Map<String, Object> mapData = new HashMap<String, Object>();
		if (size > 0 && !CommonUtils.isNotEmpty(id)) {
			mapData.put("code", "fail");
			this.renderJson(mapData);
			return;
		}
		int result = RisebdWechatReply.me.saveRes(id, keyword, type, content, title, msg_type, main_img, url);
		mapData.put("code", "success");
		if (result == 1) {
			mapData.put("result", "添加成功！！");
		} else if (result == 2) {
			mapData.put("result", "编辑成功！！");
		}
		this.renderJson(mapData);
	}

	@RequiresPermissions(value = { "/sys/wechat/reply" })
	public void delete() {
		String id = getPara("id");
		boolean result = RisebdWechatReply.me.deleteById(id);
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("code", "success");
		if (result) {
			mapData.put("result", "删除成功！！");
		} else {
			mapData.put("result", "删除失败！！");
		}
		this.renderJson(mapData);
	}

}
