<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Risebd后台管理系统</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- bootstrap & fontawesome -->
		<jsp:include page="/WEB-INF/view/common/basecss.jsp" flush="true" />
	</head>
	<body class="no-skin">
		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
		<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			<div class="main-content" id="page-wrapper">
				<div class="page-content" id="page-content">
					<div class="row">
							<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<div class="widget-box">
											<div class="widget-header widget-header-small">
												<h5 class="widget-title lighter">筛选</h5>
												<span style="padding-left: 92%"><a class="btn btn-success radius r" style="margin-top: 3px; margin-bottom: 3px; line-height: 1.2em;" href="javascript:location.replace(location.href);" title="刷新"><i class="fa fa-refresh"></i></a></span>
											</div>
											<div class="widget-body">
												<div class="widget-main">
														<div class="row">
															<div class="col-xs-12 col-sm-8">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i class="ace-icon fa fa-check"></i>
																	</span>

																	<input type="text" id="couponNo" name="couponNo" class="form-control search-query" placeholder="请输入优惠券编号" />
																	<span class="input-group-btn">
																		<button type="button" id="btn_search" class="btn btn-purple btn-sm">
																			<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																			搜索
																		</button>
																	</span>
																	
																</div>
															</div>
														</div>
												</div>
											</div>
										</div>
							</div>
							<div class="col-xs-12">
								<div class="row-fluid" style="margin-bottom: 5px;">
									
								</div>
								<!-- PAGE CONTENT BEGINS -->
								<table id="grid-table"></table>

								<div id="grid-pager"></div>

								<script type="text/javascript">
									var $path_base = "..";//in Ace demo this will be used for editurl parameter
								</script>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div>	
				</div>
			</div>
		</div><!-- /.main-container -->
		<!-- basic scripts -->
		<jsp:include page="/WEB-INF/view/common/basejs.jsp" flush="true" />
			<script type="text/javascript"> 
			function format_type(cellvalue, options, rowObject){
				if(1==cellvalue){
					return "新用户注册奖励";
				}else if(2==cellvalue){
					return "推广奖励";
				}else if(3==cellvalue){
					return "被推广奖励";
				}else if(4==cellvalue){
					return "活动自动赠送";
				}else if(5==cellvalue){
					return "活动领取";
				}else if(6==cellvalue){
					return "用于用户兑换";
				}else{
					return "未知";
				}
			}
			function format_status(cellvalue, options, rowObject){
				if(0==cellvalue){
					return "可领取";
				}else if(1==cellvalue){
					return "已领取";
				}else if(2==cellvalue){
					return "已使用";
				}else if(3==cellvalue){
					return "已过期"
				}else if(4==cellvalue){
					return "已禁止"
				}else if(5==cellvalue){
					return "已删除"
				}else{
					return "未知";
				}
				return cellvalue;
			}
			function format_form(cellvalue, options, rowObject){
				if(1==cellvalue){
					return "抵扣券";
				}else if(2==cellvalue){
					return "折扣券";
				}else{
					return "未知";
				}
			}
			var selectRowid=-1;
        $(document).ready(function () {
        	var grid_selector = "#grid-table";
			var pager_selector = "#grid-pager";
        	//resize to fit page size
			$(window).on('resize.jqGrid', function () {
				$(grid_selector).jqGrid( 'setGridWidth', $(".page-content").width() );
		    });
//resize on sidebar collapse/expand
				var parent_column = $(grid_selector).closest('[class*="col-"]');
				$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
					if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
						//setTimeout is for webkit only to give time for DOM changes and then redraw!!!
						setTimeout(function() {
							$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
						}, 0);
					}
			    });

            $("#grid-table").jqGrid({
                //url: 'http://trirand.com/blog/phpjqgrid/examples/jsonp/getjsonp.php?callback=?&qwery=longorders',
                url:'/sys/coupon/getListData',
                mtype: "GET",
                datatype: "json",
                colModel: [
					{ label: 'ID', name: 'id', key: true,hidden:true},
                    { label: '标题', name: 'risebdCouponRule.title', width: 90,sortable:false },
                    { label: '类型', name: 'risebdCouponRule.type',width: 75,formatter:format_type },
                    { label: '折扣形式', name: 'risebdCouponRule.conpon_form',width: 50,formatter:format_form },
                    { label: '优惠力度', name: 'risebdCouponRule.coupon_amount',width: 50,sortable:false },
                    { label: '订单最小金额', name: 'risebdCouponRule.limit_amount',width: 50,sortable:false },
                    { label: '优惠券编号', name: 'coupon_no',width: 80,sortable:false },
                    { label: '状态', name: 'status',width: 80,sortable:false,formatter:format_status },
                    { label: '领取会员', name: 'risebdMember.username',width: 100,sortable:false },
                    { label: '有效开始时间', name: 'risebdCouponRule.coupon_start_time',width: 100,sortable:false },
                    { label: '有效结束时间', name: 'risebdCouponRule.coupon_end_time',width: 100,sortable:false },
                ],
				viewrecords: true,
                height: 560,
                rowNum: 20,
                sortname:"id",
                sortorder:"desc",
                altRows: true,//隔行变色
                recordtext:"{0} - {1} 共 {2} 条",
                pgtext:"第 {0} 页 共 {1} 页",
                pager:pager_selector,
				onSelectRow : function( rowid ) {
					if(rowid) 
					{
						selectRowid=rowid;
						/**if(rdata.isLeaf === 'true') {
							$("#price").html(rdata.price);
							$("#uiicon").empty().append("<span class='ui-icon "+rdata.uiicon+"'></span>");
						}**/
					} 
				},
                loadComplete : function() {
					var table = this;
					setTimeout(function(){
						updatePagerIcons(table);
					}, 0);
				}
            });
			$(window).triggerHandler('resize.jqGrid');
			
			
			$("#btn_search").click(function(){  
			    //此处可以添加对查询数据的合法验证  
			    var couponNo = $("#couponNo").val();  
			    $("#grid-table").jqGrid('setGridParam',{  
			        datatype:'json',  
			        postData:{'couponNo':couponNo}, //发送数据  
			        page:1  
			    }).trigger("reloadGrid"); //重新载入  
			}); 
			
			
        });
      //replace icons with FontAwesome icons like above
		function updatePagerIcons(table) {
			var replacement = 
			{
				'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
				'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
				'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
				'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
			};
			$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
			})
		}
		/**获取选中的列***/
		function getSelectedRows() {
			var rdata = $('#grid-table').jqGrid('getRowData', selectRowid);
			return rdata;      
        }
		function setVisible(status){
			alert(getSelectedRows());
		}
		//格式化状态显示
		function fmatterStatus(cellvalue, options, rowObject){
			if(cellvalue==0){
				return '<span class="label label-sm label-warning">禁用</span>';
			}else{
				return '<span class="label label-sm label-success">启用</span>';
			}
		}
		function reloadGrid(){
			$("#grid-table").trigger("reloadGrid"); //重新载入
		}
   </script>
   		
	</body>
</html>
