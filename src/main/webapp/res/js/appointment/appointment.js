/**
 * 
 */
var doSimpleAppointmentUrl = "/appointment/doSimpleAppointment"
function doSimpleAppointment(){
	var name = $("#name").val();
	var mobile = $("#mobile").val();
	var province  = $("#province").val();
	var city  = $("#city").val();
	var area  = $("#area").val();
	var address = $("#address").val();
	var date = $("#date").val();
	var time = $("#time").val();
	var description = $("#description").val();
	if(date == ''){
		layer.msg("请选择日期", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}if(time == ''){
		layer.msg("请选择时间", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}if(name == '' || mobile=='' || city=='' || address == ''){
		layer.msg("请补全个人资料", {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}
	var params={};
	params['name'] = name;
	params['mobile'] = mobile;
	params['province'] = province;
	params['city'] = city;
	params['area'] = area;
	params['address'] = address;
	params['reservationDate'] = date;
	params['reservationTime'] = time;
	params['description'] = description;
	$.post(doSimpleAppointmentUrl,params,function(data){
		if (data.code == 'success') {
			layer.msg(data.result, {
				icon : 1,
				time : 2000
			// 2秒关闭（如果不配置，默认是3秒）
			});
			window.location.href = "/appointment/list";
		}else{
			layer.msg(data.result, {
				icon : 2,
				time : 2000
			// 2秒关闭（如果不配置，默认是3秒）
			});
		}
	});
}

function topay(appointId,appointType,nowPrice){
	if(appointType == 0 && (''==nowPrice || null == nowPrice || nowPrice==0)){
		layer.msg('一键预约订单需取货员确认价格后支付~', {
			icon : 2,
			time : 2000
		// 2秒关闭（如果不配置，默认是3秒）
		});
		return;
	}else{
		window.location.href="/pay/appointmentToPay/"+appointId;
	}
}