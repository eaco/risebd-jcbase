<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<jsp:include page="../css.jsp" flush="true" />
<title>首页</title>
</head>

<body ontouchstart>
<div class="container" id="container"></div>
<script type="text/html" id="tpl_home">
<div class="topBar">
        <a href="#/city">
                <div class="locationCity" id="currentCity" >${currentCity}</div>
        </a>
<h1 class="pageTilte">O2O洗衣</h1>
</div>
</script>
<script type="text/html" id="tpl_city">

    <div class="weui_cells_title">请选择城市</div>
    <div class="weui_cells weui_cells_radio">
    
    	<c:forEach items="${risebdCities }" var="city" varStatus="i" >
    		<label class="weui_cell weui_check_label" for="x1${i.index }">
	            <div class="weui_cell_bd weui_cell_primary">
	                <p>${city.city }</p>
	            </div>
	            <div class="weui_cell_ft">
	                <input type="radio" class="weui_check" name="city" id="x1${i.index }" value="${city.city }" onclick="changeCity('${city.city }')" <c:if test="${currentCity == city.city}">checked</c:if> >
	                <span class="weui_icon_checked"></span>
	            </div>
	        </label>
    	</c:forEach>
    </div>
</script>
<div class="bannerWrap banner-swiper-container">
	<ul class="bannerList swiper-wrapper">
		<c:forEach items="${bannerlinks }" var="bannerlink">
			
				<li class="swiper-slide"><a href="${bannerlink.link }">
				<img data-src="${ static_url}${bannerlink.coverImg }" onerror="javascript:this.src='${pageContext.request.contextPath}/res/img/default.jpg'" alt="" class="swiper-lazy"></a>
					<div class="swiper-lazy-preloader"></div>
				</li>
			
		</c:forEach>
	</ul>
	<div class="swiper-pagination"> </div>
</div>
<div class="shoppingcarBtn">
		<a href="/shoppingcar/shoppingcar">
			<i></i>
			<span>${carNum }</span>
		</a>
	</div>
<div>
	<nav class="navWrap">
				<a href="/appointment/simple" class="appointment"><span>一键</span><span>预约</span></a>
		<ul class="navList">
		
			<c:forEach items="${productTypes}" var="productType">
				<li>
					<div class="navItem">
						<a href="/product/${productType.type }">
		
							<img src="${ static_url}${productType.mainImg}" onerror= "javascript:this.src='${pageContext.request.contextPath}/res/img/default.jpg'" alt="">
							<p class="type">${productType.name }</p>
							<p class="price">${productType.description }</p>
						</a>
		
					</div>
				</li>
			</c:forEach>
		</ul>
	</nav>
	<div class="aboutServiceWrap">
		<ul class="aboutServiceList flex">
			<c:forEach items="${helpLinks }" var="helpLink">
				<li>
					<a href="${helpLink.link }"><img src="${ static_url}${helpLink.coverImg }" onerror= "javascript:this.src='${pageContext.request.contextPath}/res/img/default.jpg'" alt=""></a>
				</li>
			</c:forEach>
		</ul>
	</div>
</div>
<jsp:include page="./bottom.jsp" flush="true" />
<jsp:include page="../js.jsp" flush="true" />
</body>
<script type="text/javascript">
	//切换城市
	function changeCity(city){
		$.post("/changeCity",{"city":city},function(data){
			if(data.success){
				window.location.href="/";
			}
		});
	}

</script>
</html>