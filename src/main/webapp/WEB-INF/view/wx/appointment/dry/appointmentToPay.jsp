<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../taglib.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<jsp:include page="../css.jsp" flush="true" />
</head>

<body>
	<div class="main pFooter">
		<input type="hidden" id="openid" value="${openid}">
		<div class="payWrap">
				<div class="payHeader">
					请选择优惠券
					<select name="" id="couponSelect" class="couponSelect" onchange="couponSelectChage(${orderAmount });">
						<option value="">请选择</option>
						<option value="0">不使用</option>
						<c:forEach items="${couponMembers }" var="couponMember">
							<option value="${couponMember.couponsNo }">${memberCoupon.couponsAmount }</option>
						</c:forEach>
					</select>
				</div>
				<div class="payBody">
					<p class="payTips">请选择您的支付方式</p>
					<ul class="payWay inputBeautify">
						<li class="wechat">

							<label for="wechat">
								<input type="radio" id="wechat" name="paySelect" class="check" value="2">
								<i></i>
								<img src="/res/img/icon/wechat@2x.png" alt="">

								<span>微信支付</span>
							</label>
						</li>
						<li class="yue">
							<label for="yue">
								<input type="radio" id="yue" name="paySelect" class="check" value="1">
								<i></i>
								<img src="/res/img/icon/yue@2x.png" alt="">
								<span>余额支付</span>
							</label>
							<span>余额：${risebdMember.account }元</span>
						</li>
					</ul>
					<p class="payMuch">应付款：<i id="orderAmountI">${memberAppoint.nowPrice }</i>元</p>
				</div>
				<div class="btnWrap">
					<button type="button" class="btn btn-lg" onclick="beginPay('${memberAppoint.id}')">确认支付</button>
				</div>
		</div>
	</div>
	<jsp:include page="../js.jsp" flush="true" />
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script> 
	<script type="text/javascript" src="${res_url}js/appointment/pay.js"></script>
	<script type="text/javascript">
		wx.config({
			debug: true,
			appId:'${appid }',
			timestamp: '${timestamp}',
			nonceStr: '${nonceStr}',
			signature: '${signature}',
			jsApiList: [
	            'chooseWXPay'
	     	]
		});
	
	</script>
</body>
</html>