package com.jcbase.controller.sys;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.model.RisebdCity;
import com.jcbase.model.RisebdStore;
import com.jfinal.plugin.activerecord.Page;

public class StoreController extends JCBaseController {

	@RequiresPermissions(value = { "/sys/store" })
	public void index() {
		render("store_index.jsp");
	}

	@RequiresPermissions(value = { "/sys/store/save" })
	public void add() {
		String id = getPara("id");
		if (id != null) {
			RisebdStore risebdStore = RisebdStore.me.getById(id);
			setAttr("store", risebdStore);
		}
		List<RisebdCity> risebdCities = RisebdCity.me.getAllAvailableCity();
		setAttr("risebdCities", risebdCities);
		this.renderJsp("store_add.jsp");
	}

	@RequiresPermissions(value = { "/sys/store/delete" })
	public void delete() {
		String id = getPara("id");
		boolean result = RisebdStore.me.delById(id);
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("code", "success");
		if (result) {
			mapData.put("result", "删除成功！！");
		} else {
			mapData.put("result", "删除失败！！");
		}
		this.renderJson(mapData);
	}

	@RequiresPermissions(value = { "/sys/store/save" })
	public void save() {
		String id = getPara("id");
		String name = getPara("name");
		String main_img = getPara("main_img");
		String city = getPara("city");
		String longitude = getPara("longitude");
		String latitude = getPara("latitude");
		String address = getPara("address");
		String telphone = getPara("telphone");
		String mark = getPara("mark");
		Integer status = getParaToInt("status");
		int result = RisebdStore.me.saveStore(id, name, main_img, city, longitude, latitude, address, telphone, mark,
				status);
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("code", "success");
		if (result == 1) {
			mapData.put("result", "添加成功！！");
		} else if (result == 2) {
			mapData.put("result", "编辑成功！！");
		} else {
			mapData.put("code", "fail");
		}
		this.renderJson(mapData);
	}

	/**
	 * @category 列表
	 */
	public void getListData() {
		String name = this.getPara("name");
		Set<Condition> conditions = new HashSet<Condition>();
		if (CommonUtils.isNotEmpty(name)) {
			conditions.add(new Condition("name", Operators.LIKE, name));
		}
		Page<RisebdStore> pageInfo = RisebdStore.me.getPage(getPage(), this.getRows(), conditions, this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo));
	}
}
