package com.jcbase.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import com.jcbase.core.util.UUIDHexgenerator;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * TODO: 增加描述
 * 
 * @author yiz
 * @date 2016年8月11日 下午4:08:58
 * @version 1.0.0 
 * @copyright pycredit.cn 
 */
public class SaveTask extends RecursiveTask<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3089555479866263178L;

	private List<String> couponNos;

	private int start;

	private int end;

	private int threshold;
	private String role_id;
	private Integer status;
	private Integer coupon_type;

	public SaveTask(List<String> couponNos, int start, int end, int threshold, String role_id, Integer status,
			Integer coupon_type) {
		this.couponNos = couponNos;
		this.start = start;
		this.end = end;
		this.threshold = threshold;
		this.role_id = role_id;
		this.status = status;
		this.coupon_type = coupon_type;
	}

	@Override
	protected Integer compute() {
		int result = 0;
		if (end - start <= threshold) {
			result = this.saveCoupon(couponNos, start, end, role_id, status, coupon_type);//保存
		} else {
			int middle = (end + start) / 2;
			SaveTask leftSaveTask = new SaveTask(couponNos, start, middle, threshold, role_id, status, coupon_type);
			SaveTask rightSaveTask = new SaveTask(couponNos, middle + 1, end, threshold, role_id, status, coupon_type);
			invokeAll(leftSaveTask, rightSaveTask);
		}
		return result;
	}

	private int saveCoupon(List<String> couponNos, int start, int end, String role_id2, Integer status2,
			Integer coupon_type2) {
		List<Record> recordList = new ArrayList<>();
		Record record = null;
		for (int i = start; i <= end; i++) {
			record = new Record();
			record.set("id", UUIDHexgenerator.generate()).set("role_id", role_id).set("coupon_no", couponNos.get(i))
					.set("status", status).set("coupon_type", coupon_type).set("create_time", new Date())
					.set("update_time", new Date());
			recordList.add(record);
		}
		int[] result = Db.batchSave("risebd_coupon", recordList, recordList.size());
		if (result.length > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	/*public static void main(String[] args) {
		List<String> couponNos = new ArrayList<String>();
		couponNos.add("aa0");
		couponNos.add("aa1");
		couponNos.add("aa2");
		couponNos.add("aa3");
		couponNos.add("aa4");
		couponNos.add("aa5");
		couponNos.add("aa6");
		couponNos.add("aa7");
		couponNos.add("aa8");
		couponNos.add("aa9");
		couponNos.add("aa10");
		couponNos.add("aa11");
		ForkJoinPool forkJoinPool = new ForkJoinPool();
		SaveTask saveTask = new SaveTask(couponNos, 0, couponNos.size() - 1, 1, "asdasd", 1, 2);
		forkJoinPool.submit(saveTask);
		try {
			saveTask.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}
